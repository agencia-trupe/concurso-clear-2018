if(document.getElementById('app-vue')){

  const app = new Vue({
    el: '#app-vue',
    data() {
      return {
        form: {
          mostrar : false,
          categoria : 1
        }
      }
    },
    methods: {
      mostrarForm(cat){

        this.limparForm();

        $('html, body').stop().animate({
          scrollTop: 0
        }, 300);

        this.form.mostrar = true;
        this.form.categoria = cat;

      },
      fecharModal(){
        this.form.mostrar = false;
      },
      limparForm(){
        $("[name=autor_principal").val('');
        $("[name=co_autor_1").val('');
        $("[name=co_autor_2").val('');
        $("[name=co_autor_3").val('');
        $("[name=co_autor_4").val('');
        $("[name=co_autor_5").val('');
        $('#retorno-submissao').remove();
      },
      abrirSeHouverErros(){
        if($('#retorno-submissao').length){
          this.form.mostrar = true;
          this.form.categoria = $("[name=_categoria_old]").val();
        }
      }
    },
    mounted() {
      this.abrirSeHouverErros();
    }
  });

}
