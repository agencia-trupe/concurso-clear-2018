if(document.getElementById('app-vue-admin')){

  const app = new Vue({
    el: '#app-vue-admin',
    data() {
      return {
        isLoaded : false,
        disableAction : false,
        alerta : {
          mostrar : false,
          destino : '',
          tipo : '',
          codigo : '',
          caso_id: ''
        },
        popupRevisao : {
          show : false,
          caso : 0,
        }
      }
    },
    methods: {
      scrollToTop(){
        $('html, body').stop().animate({
          scrollTop: 0
        }, 300);
      },
      mostrarPopup(id, codigo){
        this.scrollToTop();
        this.popupRevisao.show = true;
        this.popupRevisao.caso = id;
      },
      resetPopup(){
        this.popupRevisao.caso = 0;
        $('#inputArquivo').val('');
        $('#inputJustificativa').val('');
        $('.input-arquivo .placeholder').html("[ENVIAR ARQUIVO - <strong>FORMATO PDF</strong>]")
      },
      resetAlerta() {
        this.alerta.caso_id = '';
        $('#justificativa_exclusao_input').val('');
      },
      mostrarAlerta(msg, id, codigo){

        this.scrollToTop();

        this.alerta.mostrar = true;
        this.alerta.tipo = msg;
        this.alerta.codigo = codigo;
        this.alerta.caso_id = id;

        if(msg == 'distribuir'){
          this.alerta.destino = 'distribuir-caso/' + codigo;
        }else if(msg == 'excluir'){
          this.alerta.destino = 'action:submit-form';
        }
      },
      fecharPopup(){
        this.popupRevisao.show = false;
        let self = this;
        setTimeout( function() {
          self.resetPopup();
        }, 350);
      },
      fecharModal(){
        this.alerta.mostrar = false;
        let self = this;
        setTimeout( function() {
          self.resetAlerta();
        }, 350);
      },
      goToDestination(){
        if (this.alerta.destino == 'action:submit-form') {
          $('#justificativa_exclusao_submit').off('click').trigger('click');
        } else { 
          window.location.href = this.alerta.destino;
        }
      },
      abrirSeHouverErros(){
        if($('#retorno-revisao').length){
          this.popupRevisao.show = true;
          this.popupRevisao.caso = $("[name=_caso_old]").val();
        }
      }
    },
    mounted() {
      this.isLoaded = true;
      this.abrirSeHouverErros();
    }
  });

}
