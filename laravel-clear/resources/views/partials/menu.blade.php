@if(Auth::check())
  @if( Auth::user()->isMedico )

    <nav>
      <ul>
        <li>
          <a href="regulamento" @if(Route::currentRouteName() == 'regulamento') class="ativo" @endif title="REGULAMENTO"><span>REGULAMENTO</span></a>
        </li>
        <li>
          <a href="cronograma" @if(Route::currentRouteName() == 'cronograma') class="ativo" @endif title="CRONOGRAMA"><span>CRONOGRAMA</span></a>
        </li>
        <li>
          <a href="avaliadores" @if(Route::currentRouteName() == 'avaliadores') class="ativo" @endif title="AVALIADORES"><span>AVALIADORES</span></a>
        </li>
        <li>
          <a href="submeter-caso" @if(Route::currentRouteName() == 'submeter-caso') class="ativo" @endif title="SUBMISSÃO DO CASO CLÍNICO"><span>SUBMISSÃO DO CASO CLÍNICO</span></a>
        </li>
      </ul>
    </nav>

  @elseif( Auth::user()->isCoordenador )

    <nav>
      <ul>
        <li>
          <a href="regulamento" @if(Route::currentRouteName() == 'regulamento') class="ativo" @endif title="REGULAMENTO"><span>REGULAMENTO</span></a>
        </li>
        <li>
          <a href="cronograma" @if(Route::currentRouteName() == 'cronograma') class="ativo" @endif title="CRONOGRAMA"><span>CRONOGRAMA</span></a>
        </li>
        <li>
          <a href="avaliadores" @if(Route::currentRouteName() == 'avaliadores') class="ativo" @endif title="AVALIADORES"><span>AVALIADORES</span></a>
        </li>
        <li>
          <a href="submeter-caso" @if(Route::currentRouteName() == 'submeter-caso') class="ativo" @endif title="SUBMISSÃO DO CASO CLÍNICO"><span>SUBMISSÃO DO CASO CLÍNICO</span></a>
        </li>
      </ul>
    </nav>

  @elseif( Auth::user()->isAvaliador )

    <nav>
      <ul>
        <li>
          <a href="regulamento" @if(Route::currentRouteName() == 'regulamento') class="ativo" @endif title="REGULAMENTO"><span>REGULAMENTO</span></a>
        </li>
        <li>
          <a href="cronograma" @if(Route::currentRouteName() == 'cronograma') class="ativo" @endif title="CRONOGRAMA"><span>CRONOGRAMA</span></a>
        </li>
        <li>
          <a href="avaliadores" @if(Route::currentRouteName() == 'avaliadores') class="ativo" @endif title="AVALIADORES"><span>AVALIADORES</span></a>
        </li>
        <li>
          <a href="avaliacoes" @if(Route::currentRouteName() == 'avaliacoes') class="ativo" @endif title="AVALIAÇÕES"><span>AVALIAÇÕES</span></a>
        </li>
      </ul>
    </nav>

  @elseif( Auth::user()->isAdmin )

    <nav>
      <ul>
        <li>
          <a href="novos-casos" @if(Route::currentRouteName() == 'novos-casos') class="ativo" @endif title="NOVOS CASOS CLÍNICOS + ENVIO PARA AVALIADORES"><span>NOVOS CASOS CLÍNICOS +<br> ENVIO PARA AVALIADORES</span></a>
        </li>
        <li>
          <a href="historico" @if(Route::currentRouteName() == 'historico') class="ativo" @endif title="HISTÓRICO DE AVALIAÇÕES REALIZADAS"><span>HISTÓRICO DE<br> AVALIAÇÕES REALIZADAS</span></a>
        </li>
        <li>
          <a href="status-envios" @if(Route::currentRouteName() == 'status-envios') class="ativo" @endif title="STATUS DE ENVIO DE COORDENADORES"><span>STATUS DE ENVIO<br> DE COORDENADORES</span></a>
        </li>
        <li>
          <a href="ranking" @if(Route::currentRouteName() == 'ranking') class="ativo" @endif title="RESULTADO"><span>RESULTADO</span></a>
        </li>
      </ul>
    </nav>

  @endif
@endif
