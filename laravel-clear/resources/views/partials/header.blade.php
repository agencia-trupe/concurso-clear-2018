@include('components.faixa-topo')
@include('components.faixa-usuario')

<header>
  <div class="centralizar">

    <div class="topo">
      <div class="marca-academia">
        <a href="/" title="Página Inicial">
          <img src="images/logo1.png">
        </a>
      </div>

      <div class="marca-clear">
        <a href="/" title="Página Inicial">
          <img src="images/logo2.png">
        </a>
      </div>
    </div>

    <div class="titulo">
      <h1>CONCURSO <strong>CLEAR</strong> DE CASOS CLÍNICOS 2018</h1>
    </div>

    @include('partials.menu')

  </div>
</header>
