<!DOCTYPE html>
<html lang="pt-BR" class="no-js">
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
  <meta name="robots" content="index, no-follow" />
  <meta name="viewport" content="width=device-width,initial-scale=1">

  <title>{{env('SITE_NAME')}}</title>

  <base href="{{ base_url() }}">

  <link href="https://fonts.googleapis.com/css?family=Open+Sans:300,400,700" rel="stylesheet">
  <link href="css/app.css" rel="stylesheet" type="text/css" />
</head>
<body>

  @include('partials.header')

  @yield('conteudo')

  @include('partials.footer')

  <script type="text/javascript" src="js/app.js"></script>
</body>
</html>
