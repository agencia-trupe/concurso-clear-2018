@component('mail::message')
# Novo caso clínico

## {{$titulo}}

Um novo arquivo foi enviado para revisão do administrador do Concurso Clear de Casos Clínicos. Segue anexo cópia do arquivo.

    Código do Caso Clínico: {{$codigo}}  
    Nome do médico coordenador: {{$medico}}  
    E-mail: {{$email}}  
    Data: {{$data}}  

Atenciosamente,<br>
Comissão do Concurso CLEAR
@endcomponent
