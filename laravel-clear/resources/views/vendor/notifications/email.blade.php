@component('mail::message')
{{-- Greeting --}}
@if (! empty($greeting))
# {{ $greeting }}
@else
@if ($level == 'error')
# Whoops!
@else
# Hello!
@endif
@endif

{{-- Intro Lines --}}
@foreach ($introLines as $line)
{{ $line }}

@endforeach

{{-- Action Button --}}
@isset($actionText)
<?php
    switch ($level) {
        case 'success':
            $color = 'green';
            break;
        case 'error':
            $color = 'red';
            break;
        default:
            $color = 'blue';
    }
?>
@component('mail::button', ['url' => $actionUrl, 'color' => $color])
{{ $actionText }}
@endcomponent
@endisset

{{-- Outro Lines --}}
@foreach ($outroLines as $line)
{{ $line }}

@endforeach

<!-- HTML section -->
@if (! empty($html))
{!! $html !!}
@endif

<!-- Reminder section -->
@if (! empty($reminder))
{!! $reminder !!}

@endif

<!-- Thanks section -->
@if (! empty($thanks))
{{ $thanks }}

@endif

<!-- Salutation -->
@if (! empty($salutation))
{{ $salutation }}
@elseif($salutation == 'vazio')
  <hr>
@else
Atenciosamente,<br> Comissão do Concurso CLEAR
@endif

<!-- Subcopy -->
@isset($actionText)
@component('mail::subcopy')
Se estiver encontrando problemas ao clicar em "{{ $actionText }}", copie e cole
a URL abaixo no seu navegador: [{{ $actionUrl }}]({{ $actionUrl }})
@endcomponent
@endisset
@endcomponent
