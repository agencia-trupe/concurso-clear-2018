@extends('template.index')

@section('conteudo')

  <div class="conteudo conteudo-admin com-recuoo">
    <div class="centralizar">

      <h2><span>HISTÓRICO DE CASOS CLÍNICOS NO SISTEMA &middot; AVALIAÇÕES REALIZADAS</span></h2>

      <div class="lista-avaliacoes">

        @forelse($historicoCat1 as $caso)
          <div class="linha com-titulo" data-label="CATEGORIA 1">

            <div class="avaliacao-codigo">
              <span>{{$caso->codigo}}</span>
            </div>

            @foreach($avaliadores as $k => $avaliador)
              <div class="avaliacao-status" data-label="AVALIADOR {{$k+1}}" data-nome="{{$avaliador->nome.' - '.$avaliador->cidade}}">

                @if(caso_avaliavel_por($caso->id, $avaliador->id))
                  @if($avaliador->avaliacoes()->where('casos_id', $caso->id)->first()->isAvaliado)
                    <span>
                      AVALIADO <br>{{$avaliador->avaliacoes()->where('casos_id', $caso->id)->first()->avaliado_em->format('d/m/Y')}}
                    </span>
                  @else
                    <span class="pendente">PENDENTE</span>
                  @endif

                @else

                  <span class="disabled"><small>não avalia este caso</small></span>

                @endif

              </div>
            @endforeach

          </div>
        @empty

          <div class="linha">
            <div class="nenhum">Nenhum Caso enviado para Categoria 1.</div>
          </div>

        @endforelse

      </div>

      <div class="lista-avaliacoes">

        @forelse($historicoCat2 as $caso)
          <div class="linha com-titulo" data-label="CATEGORIA 2">

            <div class="avaliacao-codigo">
              <span>{{$caso->codigo}}</span>
            </div>

            @foreach($avaliadores as $k => $avaliador)
              <div class="avaliacao-status" data-label="AVALIADOR {{$k+1}}">

                @if(caso_avaliavel_por($caso->id, $avaliador->id))
                  @if($avaliador->avaliacoes()->where('casos_id', $caso->id)->first()->isAvaliado)
                    <span>
                      AVALIADO <br>{{$avaliador->avaliacoes()->where('casos_id', $caso->id)->first()->avaliado_em->format('d/m/Y')}}
                    </span>
                  @else
                    <span class="pendente">PENDENTE</span>
                  @endif

                @else

                  <span class="disabled"><small>não avalia este caso</small></span>

                @endif

              </div>
            @endforeach

          </div>
        @empty

          <div class="linha">
            <div class="nenhum">Nenhum Caso enviado para Categoria 2.</div>
          </div>

        @endforelse

      </div>

      <div class="lista-avaliacoes">

        @forelse($historicoCat3 as $caso)
          <div class="linha com-titulo" data-label="CATEGORIA 3">

            <div class="avaliacao-codigo">
              <span>{{$caso->codigo}}</span>
            </div>

            @foreach($avaliadores as $k => $avaliador)
              <div class="avaliacao-status" data-label="AVALIADOR {{$k+1}}" data-nome="{{$avaliador->nome.' - '.$avaliador->cidade}}">

                @if(caso_avaliavel_por($caso->id, $avaliador->id))
                  @if($avaliador->avaliacoes()->where('casos_id', $caso->id)->first()->isAvaliado)
                    <span>
                      AVALIADO <br>{{$avaliador->avaliacoes()->where('casos_id', $caso->id)->first()->avaliado_em->format('d/m/Y')}}
                    </span>
                  @else
                    <span class="pendente">PENDENTE</span>
                  @endif

                @else

                  <span class="disabled"><small>não avalia este caso</small></span>

                @endif

              </div>
            @endforeach

          </div>
        @empty

          <div class="linha">
            <div class="nenhum">Nenhum Caso enviado para Categoria 3.</div>
          </div>

        @endforelse

      </div>

    </div>
  </div>

@endsection
