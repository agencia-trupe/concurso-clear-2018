@extends('template.index')

@section('conteudo')

  <div class="conteudo conteudo-admin com-recuoo" id="app-vue-admin">
    <div class="centralizar">

      <h2><span>NOVOS CASOS CLÍNICOS</span></h2>

      @if(session('sucesso'))
        <p class="alerta alerta-sucesso auto-close">
          {{session('sucesso')}}
        </p>
      @endif

      @if($errors->share->any())
        <p class="alerta alerta-erro">
          {{$errors->share->first()}}
        </p>
      @endif

      @if($errors->delete->any())
        <p class="alerta alerta-erro">
          {{$errors->delete->first()}}
        </p>
      @endif

      <p class="titulo-categoria">
        <span>CATEGORIA 1</span> Caso clínico de paciente com psoríase em placas moderada a grave, associada ou não à artrite psoriásica, em uso de secuquinumabe e que seja bio-naive (sem exposição prévia à terapia imunobiológica)
      </p>

      <div class="linha-loader" v-if="!isLoaded"></div>
      <div class="lista-casos" v-cloak>
        @forelse($novosCat1 as $caso)
          <div class="linha">
            <div class="titulo">
              <a href="download-caso/{{$caso->codigo}}" title="Fazer download do arquivo">
                <span>{{$caso->codigo}}</span>
              </a>
            </div>
            <div class="data">
              <span>
                {{$caso->enviado_em->format('d/m/Y - H:i')}}
              </span>
            </div>
            <div class="revisar">
              <a href="revisar-caso/{{$caso->codigo}}" :disabled="!isLoaded" @click.prevent="mostrarPopup('{{$caso->id}}', '{{$caso->codigo}}')">
                UPLOAD ARQUIVO REVISADO
              </a>
            </div>
            <div class="distribuir">
              <a href="distribuir-caso/{{$caso->codigo}}" :disabled="!isLoaded" @click.prevent="mostrarAlerta('distribuir', '{{$caso->id}}', '{{$caso->codigo}}')">
                DISTRIBUIR PARA AVALIADORES
              </a>
            </div>
            <div class="remover">
              <a href="excluir-caso/{{$caso->codigo}}" :disabled="!isLoaded" @click.prevent="mostrarAlerta('excluir', '{{$caso->id}}', '{{$caso->codigo}}')">
                EXCLUIR
              </a>
            </div>
          </div>
        @empty
          <div class="linha">
            <div class="nenhum">Nenhum caso a ser distribuido nesta categoria</div>
          </div>
        @endforelse
      </div>

      <p class="titulo-categoria">
        <span>CATEGORIA 2</span> Caso clínico de paciente com psoríase em placas moderada a grave, associada ou não à artrite psoriásica, em uso de secuquinumabe e com exposição prévia à terapia imunobiológica
      </p>

      <div class="linha-loader" v-if="!isLoaded"></div>
      <div class="lista-casos" v-cloak>
        @forelse($novosCat2 as $caso)
          <div class="linha">
            <div class="titulo">
              <a href="download-caso/{{$caso->codigo}}" title="Fazer download do arquivo">
                <span>{{$caso->codigo}}</span>
              </a>
            </div>
            <div class="data">
              <span>
                {{$caso->enviado_em->format('d/m/Y - H:i')}}
              </span>
            </div>
            <div class="revisar">
              <a href="revisar-caso/{{$caso->codigo}}" :disabled="!isLoaded" @click.prevent="mostrarPopup('{{$caso->id}}', '{{$caso->codigo}}')">
                UPLOAD ARQUIVO REVISADO
              </a>
            </div>
            <div class="distribuir">
              <a href="distribuir-caso/{{$caso->codigo}}" :disabled="!isLoaded" @click.prevent="mostrarAlerta('distribuir', '{{$caso->id}}', '{{$caso->codigo}}')">
                DISTRIBUIR PARA AVALIADORES
              </a>
            </div>
            <div class="remover">
              <a href="excluir-caso/{{$caso->codigo}}" :disabled="!isLoaded" @click.prevent="mostrarAlerta('excluir', '{{$caso->id}}', '{{$caso->codigo}}')">
                EXCLUIR
              </a>
            </div>
          </div>
        @empty
          <div class="linha">
            <div class="nenhum">Nenhum caso a ser distribuido nesta categoria</div>
          </div>
        @endforelse
      </div>

      <p class="titulo-categoria">
        <span>CATEGORIA 3</span> Caso clínico de paciente com psoríase em placas moderada a grave com importante acometimento do couro cabeludo e/ou lesões ungueais e/ou lesões palmoplantares em uso de secuquinumabe.
      </p>

      <div class="linha-loader" v-if="!isLoaded"></div>
      <div class="lista-casos" v-cloak>
        @forelse($novosCat3 as $caso)
          <div class="linha">
            <div class="titulo">
              <a href="download-caso/{{$caso->codigo}}" title="Fazer download do arquivo">
                <span>{{$caso->codigo}}</span>
              </a>
            </div>
            <div class="data">
              <span>
                {{$caso->enviado_em->format('d/m/Y - H:i')}}
              </span>
            </div>
            <div class="revisar">
              <a href="revisar-caso/{{$caso->codigo}}" :disabled="!isLoaded" @click.prevent="mostrarPopup('{{$caso->id}}', '{{$caso->codigo}}')">
                UPLOAD ARQUIVO REVISADO
              </a>
            </div>
            <div class="distribuir">
              <a href="distribuir-caso/{{$caso->codigo}}" :disabled="!isLoaded" @click.prevent="mostrarAlerta('distribuir', '{{$caso->id}}', '{{$caso->codigo}}')">
                DISTRIBUIR PARA AVALIADORES
              </a>
            </div>
            <div class="remover">
              <a href="excluir-caso/{{$caso->codigo}}" :disabled="!isLoaded" @click.prevent="mostrarAlerta('excluir', '{{$caso->id}}', '{{$caso->codigo}}')">
                EXCLUIR
              </a>
            </div>
          </div>
        @empty
          <div class="linha">
            <div class="nenhum">Nenhum caso a ser distribuido nesta categoria</div>
          </div>
        @endforelse
      </div>
    
    </div>  

    <transition name="fade">
      <div class="modal" v-show="alerta.mostrar" v-cloak>
        <div class="backdrop" @click.stop="fecharModal"></div>
        <div class="modal-conteudo">
          <div class="confirm-box">
            <div v-if="alerta.tipo == 'distribuir'">
              <p>
                Confirmar a distribuição do arquivo do caso clínico para os Avaliadores.
              </p>
              <p class="code"><strong>@{{alerta.codigo}}</strong></p>
            </div>
            <div v-if="alerta.tipo == 'excluir'">
              <p>
                Confirmar a exclusão do arquivo do caso clínico <strong>@{{alerta.codigo}}</strong>.
              </p>
              <form class="formCaso" id="form-exclusao-caso" action="{{route('excluir-caso')}}" method="POST">
                {!! csrf_field() !!}
                <input type="hidden" name="_caso_delete" :value="alerta.caso_id">
                <input type="hidden" name="_caso_delete_old" value="{{old('_caso_delete')}}">
                <textarea name="justificativa_exclusao" id="justificativa_exclusao_input" required placeholder="justificativa...">{{old('justificativa')}}</textarea>
                <input type="submit" style="display:none;" id="justificativa_exclusao_submit">
              </form>
            </div>            
            <div class="controles">
              <a href="#" @click.prevent="fecharModal" class="botao-voltar">CANCELAR</a>
              <a :disabled="disableAction" @click.prevent.once="goToDestination()" class="botao-envio" v-if="alerta.tipo == 'distribuir'">CONFIRMAR</a>
              <a :disabled="disableAction" @click.prevent="goToDestination()" class="botao-remover" v-if="alerta.tipo == 'excluir'">EXCLUIR</a>
            </div>
          </div>
        </div>
      </div>
    </transition>

    <transition name="fade">
      <div class="modal" v-show="popupRevisao.show" v-cloak>
        <div class="backdrop" @click.stop="fecharPopup"></div>
        <div class="modal-conteudo">
          <form class="formCaso" action="{{route('revisar-caso')}}" method="post" enctype="multipart/form-data">
            {!! csrf_field() !!}
  
            @if($errors->has('arquivo'))
              <p class="alerta alerta-erro" id="retorno-revisao">
                {{$errors->first('arquivo')}}
              </p>
            @endif

            @if($errors->has('_caso'))
              <p class="alerta alerta-erro" id="retorno-revisao">
                {{$errors->first('_caso')}}
              </p>
            @endif

            @if($errors->has('justificativa'))
              <p class="alerta alerta-erro" id="retorno-revisao">
                {{$errors->first('justificativa')}}
              </p>
            @endif
  
            <input type="hidden" name="_caso" :value="popupRevisao.caso">
            <input type="hidden" name="_caso_old" value="{{old('_caso')}}">
  
            <div class="input-arquivo">
              <label for="inputArquivo">
                <span class="placeholder">
                  [ENVIAR ARQUIVO - <strong>FORMATO PDF</strong>]
                </span>
                <span class="botao">Selecionar arquivo</span>
              </label>
              <input type="file" name="arquivo" required id="inputArquivo">
            </div>
            <textarea name="justificativa" id="inputJustificativa" placeholder="justificativa..." required>{{old('justificativa')}}</textarea>
            <input type="submit" value="ENVIAR" @click.once="">
          </form>
        </div>
      </div>
    </transition>
  </div>

@endsection
