@extends('template.index')

@section('conteudo')

  <div class="conteudo conteudo-admin com-recuoo">
    <div class="centralizar">

      <h2><span>ENVIO DE E-MAILS PARA NOVOS USUÁRIOS DO SISTEMA</span></h2>

      <a href="usuarios" class='botao-padrao'>Voltar</a>
      <a href="iniciar-usuarios" class='botao-padrao'>Reiniciar Envio</a>

      <ul class="padrao">
          {!! $body !!}
      </ul>


    </div>
  </div>

@endsection
