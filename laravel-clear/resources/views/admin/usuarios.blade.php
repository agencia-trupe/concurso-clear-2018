@extends('template.index')

@section('conteudo')

  <div class="conteudo conteudo-admin com-recuoo">
    <div class="centralizar">

      <h2><span>USUÁRIOS DO SISTEMA</span></h2>

      @if(session('sucesso'))
        <p class="alerta alerta-sucesso auto-close">
          {{session('sucesso')}}
        </p>
      @endif

      <a href="iniciar-usuarios" class='botao-padrao'>Enviar e-mails iniciais de definição de senha</a>

      <table class="lista-usuarios">
        <thead>
          <tr>
            <th>Nome</th>
            <th>E-mail</th>
            <th>Tipo</th>
            <th>Status</th>
          </tr>
        </thead>
        <tbody>
          @foreach($usuarios as $u)
            <tr>
              <td>{{$u->nome}}</td>
              <td>{{$u->email}}</td>
              <td>{{$u->tipoExtenso}}</td>
              <td class='status'>
                @if(!is_null($u->senha_criada_em))
                  <strong class="verde">Ativado em {{$u->senha_criada_em->format('d/m/Y H:i')}}</strong>
                @elseif(!is_null($u->email_enviado_em))
                  <strong class="vermelho">Inativo</strong> &bull; e-mail inicial enviado em {{$u->email_enviado_em->format('d/m/Y H:i')}} <br>
                  <a href="ativar-usuario/{{$u->email}}" title="re-enviar e-mail">re-enviar e-mail</a>
                @else
                  E-mail inicial não enviado<br>
                  <a href="ativar-usuario/{{$u->email}}" title="enviar e-mail de ativação">enviar e-mail de ativação</a>
                @endif
              </td>
            </tr>
          @endforeach
        </tbody>
      </table>


    </div>
  </div>

@endsection
