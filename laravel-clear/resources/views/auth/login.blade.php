@extends('template.index')

@section('conteudo')

  <div class="conteudo conteudo-login">
    <div class="centralizar">

      <p>
        Prezado(a) Dr(a)., seja bem vindo ao website do Concurso CLEAR de casos
        clínicos. Faço o login pra acessar mais informações.
      </p>

      <div class="form-login">
        <form action="{{ route('login') }}" method="post">
          {!! csrf_field() !!}

          @if($errors->any())
            <p class="alerta alerta-erro">
              {{$errors->first()}}
            </p>
          @endif

          @if(session('erros_sessao'))
            <p class="alerta alerta-erro">
              {{session('erros_sessao')}}
            </p>
          @endif

          @if($email)

            <p style="margin-bottom:20px;">
              SEU EMAIL: <strong>{{$email}}</strong>
            </p>
            <input type="hidden" name="email" value="{{ $email }}" required>

          @else

            <input type="email" name="email" value="{{ old('email') }}" placeholder="login (e-mail)" autofocus required>

          @endif

          <input type="password" name="password" value="{{old('password')}}" placeholder="senha" autofocus required>
          <input type="submit" value="ACESSAR SISTEMA">
        </form>
        <a href="{{ route('password.request') }}" title="esqueci minha senha" class="recuperar-senha">esqueci minha senha</a>
      </div>

    </div>
  </div>

@stop
