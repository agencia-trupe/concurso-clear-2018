@extends('template.index')

@section('conteudo')

  <div class="conteudo conteudo-login">
    <div class="centralizar">

      <p>
        Informe seu login para receber um e-mail de
        recuperação de senha no endereço cadastrado.
      </p>

      <div class="form-login">
        <form action="{{ route('password.email') }}" method="post">
          {!! csrf_field() !!}

          @if($errors->any())
            <p class="alerta alerta-erro">
              {{$errors->first()}}
            </p>
          @endif

          @if (session('status'))

            <p class="alerta alerta-sucesso">
              {{ session('status') }}
            </p>
            
          @else

            <input type="email" name="email" value="{{old('email')}}" autofocus required placeholder="login (e-mail)" >
            <input type="submit" value="ENVIAR E-MAIL DE RECUPERAÇÃO">

          @endif


        </form>
      </div>

    </div>
  </div>

@endsection
