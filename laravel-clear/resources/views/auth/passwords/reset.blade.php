@extends('template.index')

@section('conteudo')

  <div class="conteudo conteudo-login">
    <div class="centralizar">

      <p>
        Informe sua nova senha:
      </p>

      <div class="form-login">
        <form action="{{ route('password.request') }}" method="post">
          {!! csrf_field() !!}

          @if($errors->any())
            <p class="alerta alerta-erro">
              {{$errors->first()}}
            </p>
          @endif

          @if($email)
            <p style="margin-bottom:20px;">
              SEU EMAIL: <strong>{{$email}}</strong>
            </p>
            <input type="hidden" name="email" value="{{ $email }}" required>
          @else
            <input type="email" name="email" value="{{ old('email') }}" placeholder="login (e-mail)" autofocus required>
          @endif

          <input type="hidden" name="token" value="{{ $token }}">
          <input type="password" name="password" placeholder="nova senha" required>
          <input type="password" name="password_confirmation" placeholder="confirmar nova senha" required>

          <input type="submit" value="REDEFINIR SENHA">
        </form>
      </div>

    </div>
  </div>


@endsection
