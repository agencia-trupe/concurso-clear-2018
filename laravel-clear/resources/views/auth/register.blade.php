@extends('template.index')

@section('conteudo')

  <div class="conteudo conteudo-login">
    <div class="centralizar">

      <p>
        Prezado(a) Dr(a)., seja bem vindo ao website do Concurso CLEAR de casos
        clínicos. Faço o login pra acessar mais informações.
      </p>

      <div class="form-register">

        <p>
          SEU E-MAIL: <strong>{{$login}}</strong>
        </p>

        <form action="{{ route('gravar-senha') }}" method="post">
          {!! csrf_field() !!}

          @if($errors->any())
            <p class="alerta alerta-erro">
              {{$errors->first()}}
            </p>
          @endif

          <input type="hidden" name="email" value="{{$login}}" required>
          <input type="hidden" name="token" value="{{$token}}" required>
          <input type="password" name="password" placeholder="cadastrar senha" required>
          <input type="password" name="password_confirmation" placeholder="confirmar senha" required>
          <input type="submit" value="ACESSAR SISTEMA">
        </form>
      </div>

    </div>
  </div>

@stop
