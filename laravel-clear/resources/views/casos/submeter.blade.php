@extends('template.index')

@section('conteudo')
<div id="app-vue">
  <div class="conteudo conteudo-submeter-caso com-recuoo">
    <div class="centralizar reduzido">

      @if(session('sucesso'))
        <p class="alerta alerta-sucesso auto-close">
          {{session('sucesso')}}
        </p>
      @endif

      <p class="small">
        Os trabalhos deverão ser desenvolvidos no arquivo modelo conforme
        estrutura indicada e ser submetidos exclusivamente pelo website
        <a href="www.concursoclear.com.br">www.concursoclear.com.br</a> até as
        23h59 min do dia 26 de setembro de 2018.
        Não serão aceitos relatos de caso que não utilizem o arquivo modelo ou
        que tenham sido submetidos após o horário e data limites.
      </p>

      <a href="download-formulario" class="download-link" title="Download do formulário">
        <span>
          <img src="images/icone_download.png" alt="Download">
        </span>
        DOWNLOAD DO FORMULÁRIO
      </a>

    </div>
    
    <div class="centralizar">

      @if(Auth::user()->isCoordenador)

        <p>
          <strong>Envie aqui seu caso clínico.</strong>
          <br>
          IMPORTANTE: faça o download do formulário, preencha e salve em PDF, que
          deve ser o formato enviado.
        </p>

        <div class="botoes-envio">

          <div class="box">
            @if($casoCat1->isEnviado)

              <div class="casoEnviado">
                <p>
                  Arquivo do caso <strong>CATEGORIA 1</strong><br> enviado em: {{$casoCat1->enviado_em->format('d/m/Y H:i\h')}}.
                </p>
              </div>

            @else

              @if($casoCat1->podeSerEnviado && $usuarioPodeEnviar)
                <a href="#" title="Enviar caso clínico" id="mostrarFormCat1" @click.prevent="mostrarForm(1)">
                  <h3>CATEGORIA 1</h3>
                  <p>
                    Caso clínico de paciente com psoríase em placas moderada a grave, associada ou não à artrite psoriásica, em uso de secuquinumabe e que seja bio-naive (sem exposição prévia à terapia imunobiológica)
                  </p>
                  <div class="botao">
                    <span>
                      <img src="images/icone-enviar.png" alt="Enviar arquivo">
                    </span>
                    <span>
                      ENVIAR CASO CLÍNICO
                    </span>
                  </div>
                </a>

              @else
                
                <div class="prazoExpirado">
                  @if(!$casoCat1->podeSerEnviado)
                    <p class="cant-submit">
                      Sistema bloqueado para envio de casos clínicos. Em acordo com o regulamento a data de submissão é a partir das 0h
                      do dia 24 até às 23:59h do dia 26 de setembro de 2018. Em caso de dúvidas, por favor, entrar em contato pelo e-mail 
                      <a href="mailto:concurso.clear@novartis.com">concurso.clear@novartis.com</a>
                    </p>
                  @elseif(!$usuarioPodeEnviar)
                    <p>
                      Os 2 casos da participação já foram enviados.
                    </p>
                  @endif
                </div>

              @endif

            @endif
          </div>

          <div class="box">
            @if($casoCat2->isEnviado)

              <div class="casoEnviado">
                <p>
                  Arquivo do caso <strong>CATEGORIA 2</strong><br> enviado em: {{$casoCat2->enviado_em->format('d/m/Y H:i\h')}}.
                </p>
              </div>

            @else

              @if($casoCat2->podeSerEnviado && $usuarioPodeEnviar)
                <a href="#" title="Enviar caso clínico" id="mostrarFormCat2" @click.prevent="mostrarForm(2)">
                  <h3>CATEGORIA 2</h3>
                  <p>
                    Caso clínico de paciente com psoríase em placas moderada a grave, associada ou não à artrite psoriásica, em uso de secuquinumabe e com exposição prévia à terapia imunobiológica
                  </p>
                  <div class="botao">
                    <span>
                      <img src="images/icone-enviar.png" alt="Enviar arquivo">
                    </span>
                    ENVIAR CASO CLÍNICO
                  </div>
                </a>

              @else

                <div class="prazoExpirado">
                  @if(!$casoCat2->podeSerEnviado)
                    <p class="cant-submit">
                      Sistema bloqueado para envio de casos clínicos. Em acordo com o regulamento a data de submissão é a partir das 0h
                      do dia 24 até às 23:59h do dia 26 de setembro de 2018. Em caso de dúvidas, por favor, entrar em contato pelo e-mail 
                      <a href="mailto:concurso.clear@novartis.com">concurso.clear@novartis.com</a>
                    </p>
                  @elseif(!$usuarioPodeEnviar)
                    <p>
                      Os 2 casos da participação já foram enviados.
                    </p>
                  @endif
                </div>

              @endif

            @endif
          </div>

          <div class="box">
            @if($casoCat3->isEnviado)

              <div class="casoEnviado">
                <p>
                  Arquivo do caso <strong>CATEGORIA 3</strong><br> enviado em: {{$casoCat3->enviado_em->format('d/m/Y H:i\h')}}.
                </p>
              </div>

            @else

              @if($casoCat3->podeSerEnviado && $usuarioPodeEnviar)
                <a href="#" title="Enviar caso clínico" id="mostrarFormCat3" @click.prevent="mostrarForm(3)">
                  <h3>CATEGORIA 3</h3>
                  <p>
                    Caso clínico de paciente com psoríase em placas moderada a grave com importante acometimento do couro cabeludo e/ou lesões ungueais e/ou lesões palmoplantares em uso de secuquinumabe.
                  </p>
                  <div class="botao">
                    <span>
                      <img src="images/icone-enviar.png" alt="Enviar arquivo">
                    </span>
                    ENVIAR CASO CLÍNICO
                  </div>
                </a>

              @else

                <div class="prazoExpirado">
                  @if(!$casoCat1->podeSerEnviado)
                    <p class="cant-submit">
                      Sistema bloqueado para envio de casos clínicos. Em acordo com o regulamento a data de submissão é a partir das 0h
                      do dia 24 até às 23:59h do dia 26 de setembro de 2018. Em caso de dúvidas, por favor, entrar em contato pelo e-mail 
                      <a href="mailto:concurso.clear@novartis.com">concurso.clear@novartis.com</a>
                    </p>
                  @elseif(!$usuarioPodeEnviar)
                    <p>
                      Os 2 casos da participação já foram enviados.
                    </p>
                  @endif
                </div>

              @endif

            @endif
          </div>

        </div>

      @endif

    </div>
  </div>

  <transition name="fade">
    <div class="modal" v-show="form.mostrar" v-cloak>
      <div class="backdrop" @click.stop="fecharModal"></div>
      <div class="modal-conteudo">
        <form class="formCaso" action="{{route('submeter-caso')}}" method="post" enctype="multipart/form-data">
          {!! csrf_field() !!}

          @if($errors->any())
            <p class="alerta alerta-erro" id="retorno-submissao">
              {{$errors->first()}}
            </p>
          @endif

          <input type="hidden" name="_categoria" :value="form.categoria">
          <input type="hidden" name="_categoria_old" value="{{old('_categoria')}}">

          <div class="input-arquivo">
            <label for="inputArquivo">
              <span class="placeholder">
                [ENVIAR ARQUIVO - <strong>FORMATO PDF</strong>]
              </span>
              <span class="botao">Selecionar arquivo</span>
            </label>
            <input type="file" name="arquivo" required id="inputArquivo">
          </div>
          <input type="text" name="autor_principal" placeholder="Autor Principal" value="{{old('autor_principal')}}" required>
          <p>
            Garantir que o autor principal do relato de caso seja o médico que
             acompanha o paciente. O autor principal deve ser dermatologista.
             Em caso de mais de um médico acompanhando o paciente, um deles deve
             ser apontado como autor principal.
          </p>
          <input type="text" name="co_autor_1" placeholder="Co-autor 1 (se houver)" value="{{old('co_autor_1')}}">
          <input type="text" name="co_autor_2" placeholder="Co-autor 2 (se houver)" value="{{old('co_autor_2')}}">
          <input type="text" name="co_autor_3" placeholder="Co-autor 3 (se houver)" value="{{old('co_autor_3')}}">
          <input type="text" name="co_autor_4" placeholder="Co-autor 4 (se houver)" value="{{old('co_autor_4')}}">
          <input type="text" name="co_autor_5" placeholder="Co-autor 5 (se houver)" value="{{old('co_autor_5')}}">
          <input type="text" name="co_autor_6" placeholder="Co-autor 6 (se houver)" value="{{old('co_autor_6')}}">
          <input type="submit" value="ENVIAR" @click.once="">
        </form>
      </div>
    </div>
  </transition>

</div>
@endsection
