@extends('template.index')

@section('conteudo')

  <div class="conteudo conteudo-avaliadores com-recuoo">
    <div class="centralizar">

      <h2>COMITÊ AVALIADOR</h2>

      <div class="lista-avaliadores">

        <div class="linha">
          <div class="nome">Dr. André Vicente Esteves de Carvalho</div>
          <div class="crm">24016/RS</div>
          <div class="local">Porto Alegre · RS</div>
        </div>
        <div class="linha">
          <div class="nome">Dr. Aripuanã Cobério Terena</div>
          <div class="crm">28455/MG</div>
          <div class="local">Belo Horizonte · MG</div>
        </div>
        <div class="linha">
          <div class="nome">Dr. Lincoln Helder Zambaldi Fabricio</div>
          <div class="crm">15009/PR</div>
          <div class="local">Curitiba · PR</div>
        </div>
        <div class="linha">
          <div class="nome">Dra. Luiza Keiko Matsuka Oyafuso</div>
          <div class="crm">28645/SP</div>
          <div class="local">São Paulo / Santo André · SP</div>
        </div>
        <div class="linha">
          <div class="nome">Dra. Luna Azulay Abulafia</div>
          <div class="crm">349634/RJ</div>
          <div class="local">Rio de Janeiro · RJ</div>
        </div>
        <div class="linha">
          <div class="nome">Dr. Ricardo Romiti</div>
          <div class="crm">75298/SP</div>
          <div class="local">São Paulo · SP</div>
        </div>

      </div>

    </div>
  </div>

@endsection
