@extends('template.index')

@section('conteudo')

  <div class="conteudo conteudo-regulamento com-recuoo">
    <div class="centralizar">

      <h2>REGULAMENTO - CONCURSO CLEAR DE RELATOS DE CASOS CLÍNICOS 2018</h2>

      <h3>I – DO PROGRAMA</h3>

      <p>
        Promovido pela Novartis Biociências S/A, com o intuito de aprofundar a discussão científica, o Concurso CLEAR de Relatos de Casos Clínicos 2018 envolverá o envio de relatos de casos clínicos de psoríase e artrite psoriásica de pacientes em uso de secuquinumabe por grupos de trabalho organizados pela Novartis com a participação de médicos do Brasil. Website exclusivo: www.concursoclear.com.br.
      </p>

      <h3>II – DOS OBJETIVOS</h3>

      <p>
        Difundir o conhecimento técnico e científico sobre o tratamento da psoríase e artrite psoriásica com o uso de secuquinumabe entre médicos.<br>
        Permitir o intercâmbio de boas práticas médicas dirigidas à área de imunologia entre médicos dermatologistas e especialidades afins do Brasil.
      </p>

      <h3>III – DA ELEGIBILIDADE DOS PARTICIPANTES</h3>

      <p>
        Os participantes do concurso deverão atender aos seguintes critérios:
      </p>
      
      <ul>
        <li>
          Ser médico e atuar no Brasil
        </li>
        <li>
          Estar no painel de visitação da área comercial Novartis da linha Cosentyx® Dermatologia
        </li>
      </ul>

      <p>
        Os participantes do concurso serão validados e convidados pela área médica Novartis (Gerente médico e MSLs) com base nos critérios acima. Serão formados grupos de trabalho de até 7 (sete) médicos participantes, que terão encontros organizados pela área comercial Novartis para discussão de casos clínicos. O número de grupos será de 2 (dois) por consultor técnico Novartis (com exceção permitida no máximo para 3 grupos), sendo 12 consultores técnicos da linha promocional Cosentyx® Dermatologia.
      </p>

      <p>
        Os médicos inscritos assinarão termo de participação no primeiro encontro do grupo de trabalho, comprovando o atendimento aos critérios de participação.
      </p>

      <p>
        Cada grupo de trabalho terá 1 (um) coordenador (já considerado dentro do número de até 7 (sete) médicos por grupo de trabalho). O coordenador será indicado e convidado oficialmente pela área médica Novartis e estará sujeito à validação pela Área Médica da Novartis, conforme processo descrito a seguir.
      </p>

      <p>
        Os coordenadores deverão preencher um formulário padrão da Área Médica Novartis, necessário para validação de sua participação e indicação como coordenador, que será feita com base nos seguintes critérios (adicionais aos válidos para todos os participantes):
      </p>

      <ul>
        <li>
          Ter um mínimo de 5 (cinco) anos de formado como médico.
        </li>
        <li>
          Possuir título de especialista em dermatologia/especialidade afim.
        </li>
        <li>
          Ser orientador/co-orientador em pós-graduação.
        </li>
        <li>
          Possuir participação ativa (investigador principal, co-investigador, coordenador do centro etc) em pesquisa clínica nos últimos 5 anos.
        </li>
        <li>
          Ter sido membro de Advisory Board internacional nos últimos 5 anos
        </li>
        <li>
          Possuir publicação em revistas indexadas nos últimos 5 anos (autor ou coautor)
        </li>
        <li>
          Ter sido palestrante/coordenador em congresso nacional ou internacional nos últimos 5 anos
        </li>
        <li>
          Ser membro de diretoria de sociedade médica
        </li>
        <li>
          Ter tido participação como coordenador em congresso/evento local ou regional nos últimos 5 anos
        </li>
        <li>
          Ter sido palestrante em eventos de sociedade médica nos últimos 5 anos
        </li>
        <li>
          Trabalhar em centro de referência
        </li>
        <li>
          Possuir posição de liderança ou ser professor em hospital escola
        </li>
        <li>
          Ter cursado ou estar cursando pós-graduação strictu sensu (ex: mestrado ou doutorado)
        </li>
      </ul>

      <p>
        O coordenador de cada grupo terá as seguintes atribuições:
      </p>

      <ul>
        <li>
          Liderar reuniões com o seu grupo de trabalho e ser responsável pelo engajamento dos membros do grupo.
        </li>
        <li>
          Recolher o Termo de Participação do Concurso de todos os membros do grupo e entregar para o participante Novartis na primeira reunião do grupo de trabalho.
        </li>
        <li>
          Comunicar à Novartis, na primeira reunião do grupo, o número de reuniões de trabalho necessárias.
        </li>
        <li>
          Apresentar dados de secuquinumabe para o tratamento de psoríase e artrite psoriásica, utilizando o slide kit padrão enviado pela área Médica Novartis em todos os encontros presenciais.
        </li>
        <li>
          Esclarecer dúvidas dos membros do grupo sobre as regras do concurso
        </li>
        <li>
          Facilitar as discussões do grupo em relação aos casos clínicos.
        </li>
        <li>
          Assegurar que os casos discutidos não envolvem indicações não aprovadas em bula.
        </li>
        <li>
          Assegurar que os eventos adversos que possam ser identificados durante as apresentações dos casos clínicos serão relatados à Farmacovigilância da Novartis.
        </li>
        <li>
          Garantir que os casos que serão submetidos ao Concurso possuem consentimento do paciente, por escrito, para divulgação do relato e das fotos. Informações de pacientes serão anonimizadas e as fotos não permitirão a identificação do paciente.
        </li>
        <li>
          Garantir que o autor principal do relato de caso seja o médico que acompanha o paciente. O autor principal deve ser dermatologista. Em caso de mais de um médico acompanhando o paciente, um deles deve ser apontado como autor principal.
        </li>
        <li>
          Realizar a submissão dos relatos de casos clínicos escolhidos pelo grupo para o Concurso CLEAR de relatos de casos clínicos, através da plataforma online, sendo obrigatório o envio de, no mínimo, 01 caso clínico do seu grupo.
        </li>
        <li>
          Participar do treinamento Novartis sobre o produto e Compliance.
        </li>
      </ul>

      <p>
        O coordenador receberá uma remuneração nos termos da política Novartis, correspondente à sua prestação de serviço de coordenação, conforme as atribuições acima descritas.
      </p>

      <h3>IV – DO COMITÊ AVALIADOR</h3>

      <p>
        O Comitê Avaliador será formado por até 7 (sete) membros, médicos dermatologistas do Brasil, selecionados pela área médica da Novartis, os quais atuarão com total independência em relação à Novartis. Cada caso será avaliado por 2 (dois) membros do Comitê, selecionados aleatoriamente.
      </p>

      <ul>
        <li>
          Os critérios de avaliação serão (Pontuação total possível por avaliador: 100 pontos):
          <ul>
            <li>1. Título – sem pontuação</li>
            <li>2. Abstract/Resumo - 1-10 pontos (peso 1)</li>
            <li>3. Introdução - 1-10 pontos (peso 1)</li>
            <li>4. Relato do caso clínico 1-10 pontos (peso 3)</li>
            <li>5. Discussão 1-10 pontos (peso 3)</li>
            <li>6. Conclusão 1-10 pontos (peso 2)</li>
            <li>7. Referências – sem pontuação</li>
          </ul>
        </li>
        <li>
          Cada caso clínico pode somar até 200 pontos.
        </li>
        <li>
          Se existir empate após a conciliação das notas dos dois avaliadores, os critérios de desempate seguirão a seguinte sequência:
          <ul>
            <li>Maior pontuação obtida em Relato de caso clínico</li>
            <li>Maior pontuação obtida em Discussão</li>
            <li>Maior pontuação obtida em conclusão</li>
            <li>Maior pontuação em Introdução</li>
          </ul>
        </li>
      </ul>

      <p>
        Os trabalhos não serão avaliados pelo (s) membro (s) do Comitê Avaliador da mesma cidade / município do autor principal do caso do grupo de trabalho, visando garantir imparcialidade na avaliação.
      </p>
      
      <p>
        Os casos serão avaliados de forma sigilosa – o avaliador não terá informação do nome dos médicos, Hospital ou cidade do relato de caso a ser avaliado.
      </p>

      <h3>V – DO TEMA</h3>

      <p>
        O concurso será destinado aos grupos de médicos pré-selecionados conforme item II deste regulamento, que participarem das reuniões de discussão de casos clínicos organizadas pela Novartis, apresentando casos clínicos em 3 (três) categorias distintas:
      </p>

      <ul>
        <li>
          <strong>Categoria 1:</strong> Caso clínico de paciente com psoríase em placas moderada a grave, associada ou não à artrite psoriásica, em uso de secuquinumabe e que seja bio-naive (sem exposição prévia à terapia imunobiológica)
        </li>
        <li>
          <strong>Categoria 2:</strong> Caso clínico de paciente com psoríase em placas moderada a grave, associada ou não à artrite psoriásica, em uso de secuquinumabe e com exposição prévia à terapia imunobiológica
        </li>
        <li>
          <strong>Categoria 3:</strong> Caso clínico de paciente com psoríase em placas moderada a grave com importante acometimento do couro cabeludo e/ou lesões ungueais e/ou lesões palmoplantares em uso de secuquinumabe.
        </li>
      </ul>

      <p>
        Só serão aceitos casos clínicos em que o paciente estiver em uso de secuquinumabe no momento da submissão do caso clínico, para as indicações de uso descritas em bula.
      </p>

      <h3>VI – DA PREPARAÇÃO E SUBMISSÃO DOS CASOS CLÍNICOS</h3>

      <p>
        A estrutura do caso clínico deverá respeitar os campos indicados no arquivo modelo enviado via e-mail aos coordenadores, e também disponível no website do Concurso.
      </p>
      
      <p>
        A estrutura para o relato de caso clínico compreende:
        <ul>
          <li>1. Título</li>
          <li>2. Abstract/Resumo</li>
          <li>3. Introdução</li>
          <li>4. Relato do caso clínico</li>
          <li>5. Discussão</li>
          <li>6. Conclusão</li>
          <li>7. Referências</li>
        </ul>
      </p>

      <p>
        Os relatos de casos clínicos podem ter até 7 (sete) autores, sendo um indicado como autor principal, que deve ser o médico dermatologista que acompanha o paciente em questão, e os demais designados co-autores.
      </p>

      <p>
        O relato deverá preservar a identidade do médico e do Serviço/Hospital onde o paciente foi atendido, não havendo nenhuma citação ou referência ao nome, local e/ou cidade no título e corpo do texto do relato de caso.
      </p>

      <p>
        O relato não deverá conter qualquer referência ou dado pessoal de pacientes, de forma a preservar a  sua identidade. Dessa forma, não deve ser utilizado o nome ou iniciais do paciente, omitindo-se detalhes que possam identificá-lo.
      </p>        
      
      <p>
        A exibição de imagens e divulgação de informações dos pacientes será permitida apenas se for essencial para a submissão do relato de caso clínico para o Concurso CLEAR e claramente justificada pelo médico participante. Esta utilização deverá ser autorizada pelo paciente mediante o seu consentimento, expresso. As Informações de pacientes devem ser anonimizadas e as fotos não podem permitir a sua identificação. Caberá ao médico identificado como autor principal o caso clínico a responsabilidade de coletar e manter sob sua guarda o Termo de Consentimento do paciente, que não deverá ser encaminhado à Novartis.
      </p>
      
      <p>
        Não serão aceitos relatos de casos clínicos que considerem o uso de secuquinumabe para indicações e posologia diferentes daquelas aprovadas em bula no Brasil.
      </p>
      
      <p>
        Os participantes/autores serão integralmente responsáveis pela utilização e exatidão da indicação das referências bibliográficas utilizadas na elaboração do caso clínico.
      </p>
      
      <p>
        Os casos clínicos deverão ser desenvolvidos no arquivo modelo disponível no site do concurso e submetidos exclusivamente pelo website www.concursoclear.com.br, de 24 de setembro até às 23h59 min do dia 26 de setembro de 2018. Não serão aceitos relatos de caso que não utilizem o arquivo modelo ou que tenham sido submetidos após o horário e data limites.
      </p>
      
      <p>
        Poderá ser submetido somente 01 (um) relato de caso de cada categoria por grupo de trabalho; e cada grupo deve submeter um mínimo de 1 (um) e um máximo de 2 (dois) casos clínicos ao Concurso, com a limitação de, no máximo, 1 (um) caso por categoria. O caso clínico deve ser enviado dentro da data limite: 24 a 26 de setembro de 2018.
      </p>

      <h3>VII – DA DINÂMICA DO CONCURSO E DIVULGAÇÃO DO RESULTADO</h3>

      <p>
        De acordo com os critérios definidos na sessão III, serão formados grupos de discussão de casos clínicos a partir de convite da área médica e comercial Novartis. Cada grupo terá até 7 (sete) participantes, sendo um coordenador, que ficará responsável pelo alinhamento e coordenação dos trabalhos de seu grupo.
      </p>

      <p>
        Entre 23 de abril e 31 de agosto de 2018, com suporte e organização da Novartis (locação de sala para evento, equipamentos, refeição, logística de participantes e remuneração do coordenador), serão realizadas reuniões dos grupos de discussão para o debate sobre casos clínicos. Poderão ser realizadas entre 2 e 3 reuniões por grupo, o coordenador deverá comunicar à Novartis o número de encontros planejados. Ao final das reuniões, cada grupo selecionará no mínimo 1 (um) e no máximo 2 (dois) casos, com a limitação de, no máximo, 1 (um) caso por categoria, que deverão ser submetidos ao concurso CLEAR.
      </p>

      <p>
        Representantes da Novartis estarão presentes nas reuniões, porém não terão qualquer ingerência ou interferência nas discussões dos grupos de trabalho, a não ser sob solicitação e caso possam colaborar com o grupo, dentro das limitações de informações dos presentes, considerando o cargo de cada      representante Novartis presente na reunião.
      </p>
      
      <p>
        A área médica dará o suporte para o conteúdo científico a ser apresentado nas reuniões. O conteúdo a ser apresentado pelos coordenadores nos grupos de trabalho estará restrito ao slide kit padrão enviado pela área Médica Novartis, que será dividido em 2 ou 3 aulas, de acordo com o número de encontros de cada grupo. Adicionalmente a este conteúdo, serão apresentados somente os casos clínicos de secuquinumabe de cada participante para discussão e escolha de um ou dois casos a serem submetidos ao concurso.
      </p>

      <p>
        Cada grupo deverá realizar entre 2 a 3 reuniões, considerando as dinâmicas descritas abaixo:
      </p>

      <ul>
        <li>
          Reunião 1: O coordenador recolhe o formulário de participação, apresenta o regulamento do Concurso aos participantes e apresenta os slides de secuquinumabe (slide kit padrão, parte 1). O coordenador e participantes do grupo apresentam seus casos clínicos para discussão e seleção dos principais casos.
        </li>
        <li>
          Reunião 2: O coordenador faz a apresentação dos slides de secuquinumabe (slide kit padrão, parte 2). Seguem com as discussões sobre os casos de destaque e definem um ou dois casos que serão submetidos e a quais categorias.
        </li>
        <li>
          Reunião 3*: O coordenador faz a apresentação dos slides de secuquinumabe (slide kit padrão, parte 3). Finalizam as discussões sobre os casos que serão submetidos ao Concurso.
        </li>
      </ul>

      <p>
        *A reunião 3 ocorrerá apenas caso seja necessária. Se houverem somente 2 reuniões, na segunda reunião serão apresentados os slides kits 2 e 3.
      </p>

      <p>
        Os casos serão avaliados de forma sigilosa através de ferramenta de votação, seguindo os critérios mencionados na sessão VII. Serão escolhidos 3 grupos vencedores do concurso, sendo eles:
      </p>
      
      <ul>
        <li>Vencedor 1: Caso com maior pontuação na categoria 1</li>
        <li>Vencedor 2: Caso com maior pontuação na categoria 2</li>
        <li>Vencedor 3: Caso com maior pontuação na categoria 3</li>
      </ul>

      <p>
        Os vencedores serão divulgados durante o Fórum Novartis (evento de encerramento do concurso) no dia 10 de Novembro de 2018. Para este evento, todos os grupos de trabalho (coordenadores + participantes), além dos membros do comitê avaliador, serão convidados e a Novartis oferecerá apoio logístico com hospedagem e transporte. Serão desclassificados os grupos que não estiverem representados com pelo menos 2 (dois) de seus membros no Fórum Novartis (evento de encerramento do concurso), e um novo grupo vencedor daquela categoria será anunciado com base nos critérios descritos na sessão VIII.
      </p>

      <p>
        Todos os casos clínicos submetidos ao Concurso poderão ser divulgados/exibidos na íntegra ou adaptados em formato editado (pôster, folder, etc.) durante eventos Novartis, bem como no todo ou em parte para a elaboração de materiais promocionais da Novartis, com a correspondente indicação da autoria. A assinatura do termo de participação formaliza o consentimento do autor com a utilização do conteúdo descrita nesse regulamento.
      </p>

      <p>
        Além disso, os vencedores poderão ser convidados pela Novartis para apresentar seus relatos de casos clínicos em eventos Novartis.
      </p>

      <h3>VIII – DA PREMIAÇÃO</h3>

      <p>
        Só serão elegíveis à premiação os grupos que estiverem representados por pelo menos dois membros no fórum de encerramento Novartis (evento de encerramento do concurso), caso contrário, o grupo será desclassificado e o próximo melhor colocado na categoria ganhará o prêmio, desde que tenha 2 participantes presentes, e essa regra será válida até que seja premiado um grupo elegível.
      </p>

      <p>
        Os grupos vencedores (coordenador e demais membros do grupo) receberão o apoio educacional para a participação no Congresso Brasileiro de Dermatologia 2019, além de terem o caso clínico comentado publicado em material promocional desenvolvido pela Novartis.
      </p>

      <p>
        O apoio educacional acima mencionado (exclusivamente para os membros e coordenadores dos grupos vencedores) é de caráter pessoal e intransferível para cada membro do grupo e não poderá ser convertido em dinheiro ou substituído por qualquer outra forma de compensação material. Consiste em inscrição para o congresso e passagem aérea em classe econômica ou transporte terrestre em veículo automotor (dependendo da localização de residência) e hospedagem em apartamento single e restringe-se exclusivamente aos membros do grupo vencedor, não sendo extensivo a acompanhantes.
      </p>

      <p>
        Quaisquer despesas incorridas pelos participantes que receberem o apoio educacional acima descrito durante sua participação no Congresso serão de sua única e exclusiva responsabilidade.
      </p>

      <p>
        Os relatos selecionados também serão utilizados para elaboração de um material promocional para cada grupo vencedor (total de 3 materiais). Este material será escrito por todos os membros do grupo vencedor, e conterá: transcrição do relato do caso clínico vencedor e inclusão de comentários sobre temas associados ao caso, indicados pela Novartis. Cada integrante do grupo receberá pagamento de honorários de R$ 3.500 (três mil e quinhentos reais, valor bruto, sendo que serão deduzidos impostos) para sua participação no desenvolvimento do material promocional. Caso algum integrante não queira participar como autor do material, deverá manifestar esta recusa expressamente, e não fará jus ao recebimento dos honorários previstos. O material será produzido e divulgado em 2019, e a elaboração e pagamento do honorário serão objeto de contrato específico.
      </p>

      <h3>IX – DO CRONOGRAMA*</h3>

      <ul>
        <li>
          Março: prazo para validações dos médicos participantes pela área médica Novartis, e convite dos mesmos.
        </li>
        <li>
          14 de abril: reunião presencial com os coordenadores e membros da comissão julgadora para apresentação do programa.
        </li>
        <li>
          De 23 de abril a 31 de agosto: reuniões dos grupos de trabalho para discussão dos casos clínicos.
        </li>
        <li>
          De 24 a 26 de setembro: submissão dos casos clínicos na plataforma para avaliação do comitê.
        </li>
        <li>
          De 02 de outubro a 31 de outubro: avaliação dos relatos de casos clínicos pelo comitê avaliador.
        </li>
        <li>
          Dia 10 de Novembro: Fórum de encerramento do concurso com divulgação dos vencedores e cerimônia de premiação.
        </li>
        <li>
          2019: Premiação - publicação de material promocional com caso clínico comentado para cada grupo vencedor e participação no Congresso Brasileiro de Dermatolog
        </li>
      </ul>

      <p>*Datas poderão sofrer alteração mediante comunicação aos participantes</p>

      <h3>X – CONSIDERAÇÕES FINAIS</h3>

      <p>
        Ao inscreverem-se para o Concurso CLEAR de Relatos de Casos Clínicos 2018 os participantes tacitamente concordam com as regras constantes deste documento, bem como a utilização sem ônus e sem qualquer contrapartida pela Novartis, de seu respectivo nome, imagem e trabalho para divulgação em qualquer meio de comunicação e publicações científicas, em âmbito nacional ou internacional, conforme legislação vigente, assim como publicação em materiais científicos e eventos da Novartis, que se compromete a identificar a autoria do conteúdo divulgado. Não se inserem nas disposições contidas neste artigo as atividades relacionadas à elaboração de material promocional, descritas no item.
      </p>
      
      <p>
        As opiniões ou declarações contidas nos relatos de casos clínicos são de responsabilidade única e exclusiva dos respectivos autores e não refletem necessariamente a posição da Novartis.
      </p>

      <p>
        Suspeitas de conduta antiética entre os participantes do Concurso e na elaboração dos casos clínicos serão apreciadas pelo Comitê Avaliador e, se consideradas procedentes, acarretarão sua desclassificação.
      </p>

      <p>
        Os relatos de casos clínicos que não estejam de acordo com este regulamento serão desclassificados, mediante comunicação com os motivos para desclassificação aos participantes.
      </p>

      <p>
        Não se estabelece, pela força dessa iniciativa, de caráter essencialmente científico e educacional, qualquer vínculo de qualquer natureza entre a Novartis e os participantes.
      </p>

      <p>
        A Novartis não será responsável por nenhuma despesa incorrida pelo participante deste Concurso para a elaboração e envio dos seus trabalhos ou por qualquer ato do participante que possa causar danos materiais ou morais a sua pessoa ou de terceiros.
      </p>

      <p>
        O Participante responderá integralmente pela veracidade e acuidade dos dados cadastrados e enviados, incluindo os dados constantes do relato de caso inscrito, sua originalidade e exatidão em relação referências utilizadas e indicadas no material submetido.
      </p>

      <p>
        A Novartis se reserva no direito de alterar este regulamento a qualquer tempo, mediante notificação prévia a todos os participantes.
      </p>

      <p>
        Quaisquer questões relativas a esta iniciativa de caráter científico e educacional que não estejam endereçadas no presente Regulamento, serão avaliadas pelo Comitê Avaliador, cuja decisão será soberana, não sendo admitido questionamento posterior.
      </p>

      <p>
        <i>
          Regulamento Versão 1.0<br>
          São Paulo, 16 de março de 2018
        </i>
      </p>

      <p>
        <small>
            Realização: NOVARTIS BIOCIÊNCIAS S/A<br>
            Setor Farma – Av Prof. Vicente Rao, 90 São Paulo, SP – CEP 04636-000<br>
            www.novartis.com.br www.portal.novartis.com.br<br>
            SIC – Serviço de Informação ao Cliente 0800 888 3003 sic.novartis@novartis.com<br>
            Material produzido em março de 2018.<br>
            Material destinado a médicos participantes do concurso CLEAR de casos clínicos 2018.<br>
            6355907 KC REGULAMENTO CONC CLEAR 0,001 0318 BR
        </small>
      </p>

    </div>
  </div>

@endsection
