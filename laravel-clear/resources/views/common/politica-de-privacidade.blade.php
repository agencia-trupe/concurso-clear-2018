@extends('template.index')

@section('conteudo')

  <div class="conteudo conteudo-regulamento @if(Auth::check()) com-recuoo @endif">
    <div class="centralizar">

      <h2>POLÍTICA DE PRIVACIDADE</h2>

      <h3>1 - Compromisso com a Privacidade</h3>

      <p>
        O termo "dados pessoais", na acepção desta Política, refere-se a informações como o nome, data de nascimento, endereço eletrônico ou endereço postal do usuário, que poderão ser utilizadas para identificá-lo(a). A Novartis não fará processamento dos dados pessoais do usuário sem sua autorização. Ao processar os dados pessoais do usuário, comprometemo-nos a cumprir integralmente as normas de proteção à privacidade reconhecidas internacionalmente. Ao cumprir as referidas normas, a Novartis garante o cumprimento, por nosso pessoal, de padrões rígidos de segurança e sigilo. As seções abaixo explicitam como e quando a Novartis coleta dados pessoais de usuários.
      </p>

      <h3>2 - Uso Pretendido de Dados Pessoais</h3>

      <p>
        A maioria de nossos serviços não requer qualquer forma de cadastramento, permitindo ao usuário a visita ao Site da Novartis sem informar sua identidade. Alguns serviços, entretanto, poder exigir cadastramento. Ao fazer o cadastramento com a Novartis, o usuário poderá ser solicitado a preencher alguns campos (alguns obrigatórios e outros opcionais), bem como escolher um nome e senha. Sob essas circunstâncias, caso o usuário opte por recusar-se a fornecer os dados pessoais solicitados, essa recusa poderá implicar em negativa de acesso a determinadas partes deste site e na impossibilidade de resposta a consultas. A Novartis processa dados pessoais para fins específicos e limitados, os quais serão informados ao usuário na medida da solicitação pela Novartis das referidas informações. As informações transmitidas pelo usuário ao nosso departamento de serviço ao cliente serão utilizadas para ajudar a resolver o problema do usuário e será, outrossim, mantida em sigilo. A Novartis manterá os dados do usuário pelo período de tempo justificadamente necessário para esse fim específico e de acordo com requisitos vigentes, sejam técnicos ou na forma da lei, relativos à retenção de documentos.
      </p>

      <h3>3 - Não Divulgação das Informações</h3>

      <p>
        A Novartis compromete-se a não comercializar, compartilhar ou outrossim distribuir dados pessoais de usuários a terceiros estranhos à Novartis AG. Entretanto, poderá haver ocasionalmente transferência de dados para terceiros na medida em que venham a trabalhar junto à Novartis ou em seu nome ou em decorrência dos negócios da Novartis, para posterior processamento dentro do(s) propósito(s) para os quais os dados foram originalmente coletados. Na medida da necessidade da disponibilização de dados para terceiros por qualquer motivo a Novartis deverá, dentro do possível, certificar-se de que a referida disponibilização e finalidade do uso dos dados sejam claramente expressos. As referidas partes poderão funcionar sob políticas de privacidade diversas. Entretanto, a Novartis envidará esforços para garantir que as referidas partes proporcionem o mesmo nível de proteção oferecido pela Novartis e, onde exeqüível, será exigido na forma de contrato o processamento dos dados transmitidos exclusivamente para os fins autorizados por escrito pela Novartis. A Novartis não compartilhará com terceiros quaisquer dados sobre o usuário que sejam considerados "suscetíveis" sem a autorização prévia e expressa do usuário. A autorização do usuário é passível de revogação em data posterior. Em caso de revogação da referida autorização, a Novartis poderá ficar impedida de atender determinadas solicitações do usuário. Dentro do possível, a Novartis informará as partes às quais seus dados foram transferidos a revogação da mesma.
      </p>

      <h3>4 - Direito de Acesso</h3>

      <p>
        O usuário tem direito de acessar e atualizar seus dados pessoais ou solicitar sua exclusão. A Novartis envidará esforços para garantir que os dados pessoais estejam atualizados, precisos e completos. Caso o usuário deseje acessar ou corrigir seus dados pessoais em poder da Novartis, deverá entrar em contato com o webmaster. A solicitação do usuário será tratada de maneira imediata e adequada. Não será cobrada qualquer taxa para executar a correção, entretanto, para outras solicitações a Novartis poderá cobrar uma taxa para cobrir seus custos. As solicitações de exclusão de dados pessoais ficam sujeitas a notificações éticas ou protocolo de documentos ou obrigações de retenção impostas à Novartis.
      </p>

      <h3>5 - Segurança e Sigilo</h3>

      <p>
        Para garantir a segurança e o sigilo de dados pessoais cadastrados com a Novartis on-line, a mesma utiliza, entre outros, um firewall (sistema de segurança) padrão da indústria e proteção por senha. O acesso a dados pessoais fica restrito aos funcionários que obrigatoriamente utilizam os dados e que receberam treinamento adequado para o tratamento correto dos mesmos, observando rigorosamente as normas de sigilo. Caso um funcionário venha a transgredir as políticas e procedimento da Novartis, o mesmo sofrerá as sanções previstas para tais casos. O cumprimento pelos funcionários das políticas e procedimentos da Novartis é periodicamente auditado e verificado. Embora a Novartis não possa garantir contra perda, mau uso ou alteração de dados, tenta impedir as referidas ocorrências.
      </p>

      <h3>6 - Transferência de Dados para o Exterior</h3>

      <p>
        A Novartis é uma empresa multinacional e possui bancos de dados em diversas jurisdições. A Novartis poderá transferir os dados de usuários para um de seus bancos de dados fora do país de domicílio dos referidos usuários. Caso o nível de proteção da privacidade em um determinado país não esteja em conformidade com normas reconhecidas internacionalmente, a Novartis garante que as transferências de dados para os bancos de dados da Novartis, no referido país, estejam adequadamente protegidas e que a transferência de dados a terceiros no mesmo país não ocorra salvo mediante autorização prévia e expressa do usuário.
      </p>

      <h3>7 - Dados Anônimos e Cookies</h3>

      <p>
        A maioria das informações coletadas pela Novartis através de seu Website são informações anônimas, como por exemplo as páginas visitadas ou buscas realizadas pelo usuário. Ao visitar o Website da Novartis não há coleta de dados pessoais do usuário, salvo se expressamente autorizada pelo mesmo. As informações anônimas são processadas pela Novartis com o intuito de ajudar a melhorar o conteúdo do site e para compilar dados estatísticos acumulados sobre os visitantes do site da Novartis e que usam o site para fins internos e de pesquisa de mercado. Nesse caso, a Novartis poderá instalar cookies que coletam informações sobre o domínio de primeiro nível do usuário, bem como a data e o momento do acesso. Os cookies em si não podem ser utilizados para descobrir a identidade do usuário. Um cookie é uma pequena informação contida em um arquivo texto enviada ao navegador do usuário e armazenada no disco rígido de seu computador. Os Cookies não prejudicam o funcionamento do computador. O usuário pode configurar seu navegador para notificá-lo do recebimento de um Cookies, dando ao usuário a opção de aceitá-los ou não.
      </p>

      <h3>8 - Spams</h3>

      <p>
        A Novartis não é conivente com a prática de envio de spams. A prática de envio de spams pode ser definida como a transmissão de e-mails não solicitados, geralmente de cunho comercial, em grandes quantidades e repetidamente a pessoas com as quais o remetente não teve qualquer contato prévio ou que declinaram receber as referidas mensagens. Do contrário, desde que a Novartis acredite que determinadas informações sobre produtos, saúde ou outras informações possam ter importância para o usuário, a mesma reserva o direito de passar essa informação ao usuário via e-mail dando, ao mesmo tempo, a opção de cancelar esse serviço.
      </p>

      <h3>9 - Informações Pessoais e Menores de Idade</h3>

      <p>
        A Novartis, desde que tenha conhecimento, não coleta, utiliza ou divulga dados pessoais de jovens abaixo de 18 anos sem o consentimento prévio do pai ou responsável através de contado direto off-line. A Novartis apresentará ao pai ou responsável (i) uma notificação com os tipos específicos de informações pessoais a serem obtidas a respeito do menor; e (ii) a oportunidade de fazer objeções a qualquer coleta, uso ou armazenamento posterior das referidas informações. A Novartis respeita a legislação de nome Children's Online Privacy Protection Act (Lei de Proteção à Privacidade Infantil Online") vigente nos EUA, bem como leis similares vigente em outros países com o intuito de proteger a infância.
      </p>

      <h3>10 - Links para Outros Sites</h3>

      <p>
        Esta Política de Privacidade se aplica exclusivamente aos websites da Novartis, excluindo qualquer outro website de terceiros. A Novartis poderá exibir links a outros websites, na medida que os mesmos possam ser de interesse dos seus usuários. A Novartis procura garantir o alto nível dos referidos websites. Entretanto, devido à Natureza da Internet, a Novartis fica impossibilitada de garantir o padrão de todos os links que oferece ou tampouco responder pelo conteúdo de sites de terceiros.
      </p>

      <h3>11 - Fale com a Novartis</h3>

      <p>
        Caso o usuário tenha consultas ou reclamações a fazer sobre a conformidade com esta Política de Privacidade, ou se desejar fazer recomendações ou comentários com vistas à melhoria da qualidade de nossa Política de Privacidade, favor entrar em contato com o Webmaster. Esta política de privacidade entrou em vigor em 1° de janeiro de 2001. Nosso objetivo é a melhoria constante das ferramentas disponibilizadas aos usuários para poder administrar os dados transmitidos pelos mesmos. Favor mencionar esta página ao analisar estes ou novos recursos.
      </p>
      
    </div>
  </div>

@endsection
