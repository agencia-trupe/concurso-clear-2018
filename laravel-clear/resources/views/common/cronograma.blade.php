@extends('template.index')

@section('conteudo')

  <div class="conteudo conteudo-cronograma com-recuoo">
    <div class="centralizar">

      <div class="lista-cronograma">

        <div class="linha">
          <div class="data">
            <span>
                Março:
            </span>
          </div>
          <div class="texto">
            <p>
                prazo para validações dos médicos participantes pela área médica Novartis, e convite dos mesmos.
            </p>
          </div>
        </div>

        <div class="linha">
          <div class="data">
            <span>
                14 de abril:
            </span>
          </div>
          <div class="texto">
            <p>
                reunião presencial com os coordenadores e membros da comissão julgadora para apresentação do programa.
            </p>
          </div>
        </div>

        <div class="linha">
          <div class="data">
            <span>
                De 23 de abril a 31 de agosto:
            </span>
          </div>
          <div class="texto">
            <p>
                reuniões dos grupos de trabalho para discussão dos casos clínicos.
            </p>
          </div>
        </div>
        
        <div class="linha">
          <div class="data">
            <span>
                De 24 a 26 de setembro:
            </span>
          </div>
          <div class="texto">
            <p>
                submissão dos casos clínicos na plataforma para avaliação do comitê.
            </p>
          </div>
        </div>

        <div class="linha">
          <div class="data">
            <span>
                De 02 de outubro a 31 de outubro:
            </span>
          </div>
          <div class="texto">
            <p>
                avaliação dos relatos de casos clínicos pelo comitê avaliador.
            </p>
          </div>
        </div>        

        <div class="linha">
          <div class="data">
            <span>
                Dia 10 de Novembro:
            </span>
          </div>
          <div class="texto">
            <p>
                Fórum de encerramento do concurso com divulgação dos vencedores e cerimônia de premiação.
            </p>
          </div>
        </div>
        
        <div class="linha">
          <div class="data">
            <span>
                2019
            </span>
          </div>
          <div class="texto">
            <p>
                Premiação - publicação de material promocional com caso clínico comentado para cada grupo vencedor e participação no Congresso Brasileiro de Dermatolog        
            </p>
          </div>
        </div>
        
        <p class="aviso">
          Observação: Datas poderão sofrer alteração mediante comunicação aos participantes.
        </p>
        
    </div>
  </div>

@endsection
