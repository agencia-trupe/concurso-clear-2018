@extends('template.index')

@section('conteudo')

  <div class="conteudo conteudo-home com-recuoo">
    <div class="centralizar">

      @if(session('sucesso_criacao_senha'))
        <p class="alerta alerta-sucesso" style="margin-bottom: 30px;">
          {{session('sucesso_criacao_senha')}}
        </p>
      @endif

      <p>
        <i>
          Promovido pela Novartis Biociências S/A, o Concurso CLEAR de Relatos de Casos Clínicos 2018 envolverá o envio de casos clínicos de psoríase, associados ou não com artrite psoriásica, de pacientes em uso de secuquinumabe.
        </i>
      </p>

      <div class="box-objetivos">
        <h2>Objetivos:</h2>
        <p>
          Difundir o conhecimento técnico e científico sobre o tratamento da psoríase e artrite psoriásica com o uso de secuquinumabe entre médicos.
        </p>
        <p>
          Permitir o intercâmbio de boas práticas médicas dirigidas à área de imunologia entre médicos dermatologistas e especialidades afins do Brasil.
        </p>
      </div>

    </div>
  </div>

@endsection
