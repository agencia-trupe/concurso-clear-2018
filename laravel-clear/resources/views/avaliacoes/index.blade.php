@extends('template.index')

@section('conteudo')

  <div class="conteudo conteudo-avaliacoes com-recuoo" id="app-vue-avaliador">
    <div class="centralizar">

      <h2>RELATOS DE CASOS CLÍNICOS PARA AVALIAÇÃO</h2>

      <div class="contem-colunas">
        <div class="coluna coluna-55">
          <div class="texto-avaliacao">
            <p>
              Insira sua pontuação de 1 a 10 (número inteiro com uma casa decimal)
               em cada critério dos casos clínicos para sua avaliação
            </p>
            <p>
              Você pode editar as notas, basta preencher novamente e clicar em
              "salvar". Essa edição é possível até o dia <strong>{{$dataMaxAValiacao}}
               às 23:59.</strong> Após isso, o sistema será bloqueado e as notas salvas
               serão as consideradas.
            </p>
          </div>
        </div>
        <div class="coluna coluna-45">
          <div class="destaque">
            <p>
              Os critérios de avaliação são (Pontuação total possível por avaliador: 100 pontos)
            </p>
            <ol>
              <li>Título – sem pontuação</li>
              <li>Abstract/Resumo - 1-10 pontos (peso 1)</li>
              <li>Introdução - 1-10 pontos (peso 1)</li>
              <li>Relato do caso clínico 1-10 pontos (peso 3)</li>
              <li>Discussão 1-10 pontos (peso 3)</li>
              <li>Conclusão 1-10 pontos (peso 2)</li>
              <li>Referências – sem pontuação</li>              
            </ol>
          </div>
        </div>
      </div>

      <p class="titulo-categoria">
        <span>CATEGORIA 1</span> Caso clínico de paciente com psoríase em placas moderada a grave, associada ou não à artrite psoriásica, em uso de secuquinumabe e que seja bio-naive (sem exposição prévia à terapia imunobiológica)
      </p>

      <div class="lista-avaliacoes">
        <div class="linha-loader" v-if="!isLoaded && avaliacoes.novas.categoria1.length == 0"></div>

        <div v-if="isLoaded && avaliacoes.novas.categoria1.length == 0" v-cloak class="linha">
          <div class="nenhum">Nenhum novo caso para ser Avaliado nesta Categoria</div>
        </div>

        <linha v-for="linha in avaliacoes.novas.categoria1" :key="linha.id" :linha="linha" @sucesso="buscarAvaliacoes">
        </linha>
      </div>

      <p class="titulo-categoria">
        <span>CATEGORIA 2</span> Caso clínico de paciente com psoríase em placas moderada a grave, associada ou não à artrite psoriásica, em uso de secuquinumabe e com exposição prévia à terapia imunobiológica
      </p>

      <div class="lista-avaliacoes">
        <div class="linha-loader" v-if="!isLoaded && avaliacoes.novas.categoria2.length == 0"></div>

        <div v-if="isLoaded && avaliacoes.novas.categoria2.length == 0" v-cloak class="linha">
          <div class="nenhum">Nenhum novo caso para ser Avaliado nesta Categoria</div>
        </div>

        <linha v-for="linha in avaliacoes.novas.categoria2" :key="linha.id" :linha="linha" @sucesso="buscarAvaliacoes">
        </linha>
      </div>

      <p class="titulo-categoria">
        <span>CATEGORIA 3</span> Caso clínico de paciente com psoríase em placas moderada a grave com importante acometimento do couro cabeludo e/ou lesões ungueais e/ou lesões palmoplantares em uso de secuquinumabe.
      </p>

      <div class="lista-avaliacoes">
        <div class="linha-loader" v-if="!isLoaded && avaliacoes.novas.categoria3.length == 0"></div>

        <div v-if="isLoaded && avaliacoes.novas.categoria3.length == 0" v-cloak class="linha">
          <div class="nenhum">Nenhum novo caso para ser Avaliado nesta Categoria</div>
        </div>

        <linha v-for="linha in avaliacoes.novas.categoria3" :key="linha.id" :linha="linha" @sucesso="buscarAvaliacoes">
        </linha>
      </div>      

      <div v-if="avaliacoes.historico.categoria1.length > 0 || avaliacoes.historico.categoria2.length > 0 || avaliacoes.historico.categoria3.length > 0">

        <h2 class="titulo-historico">HISTÓRICO DE AVALIAÇÕES REALIZADAS</h2>

        <div class="lista-avaliacoes">
          <div class="linha-loader" v-if="!isLoaded && avaliacoes.historico.categoria1.length == 0"></div>

          <div v-if="isLoaded && avaliacoes.historico.categoria1.length == 0" v-cloak class="linha">
            <div class="nenhum">Nenhum caso avaliado na Categoria 1</div>
          </div>

          <linha-editavel v-for="linha in avaliacoes.historico.categoria1" :key="linha.id" :cat="1" :linha="linha" @sucesso="buscarAvaliacoes">
          </linha>
        </div>

        <div class="lista-avaliacoes">
          <div class="linha-loader" v-if="!isLoaded && avaliacoes.historico.categoria2.length == 0"></div>

          <div v-if="isLoaded && avaliacoes.historico.categoria2.length == 0" v-cloak class="linha">
            <div class="nenhum">Nenhum caso avaliado na Categoria 2</div>
          </div>

          <linha-editavel v-for="linha in avaliacoes.historico.categoria2" :key="linha.id" :cat="2" :linha="linha" @sucesso="buscarAvaliacoes">
          </linha>
        </div>

        <div class="lista-avaliacoes">
          <div class="linha-loader" v-if="!isLoaded && avaliacoes.historico.categoria3.length == 0"></div>

          <div v-if="isLoaded && avaliacoes.historico.categoria3.length == 0" v-cloak class="linha">
            <div class="nenhum">Nenhum caso avaliado na Categoria 3</div>
          </div>

          <linha-editavel v-for="linha in avaliacoes.historico.categoria3" :key="linha.id" :cat="3" :linha="linha" @sucesso="buscarAvaliacoes">
          </linha>
        </div>

      </div>


    </div>
  </div>

@endsection
