<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Auth::routes();
Route::get('register', 'Auth\RegisterController@register'); // rota desativada
Route::post('register', 'Auth\RegisterController@postRegister'); // rota desativada

Route::get('criar-senha/{token}/{login}', ['as' => 'criar-senha', 'uses' => 'Auth\LoginController@getCriarSenha']);
Route::post('criar-senha', ['as' => 'gravar-senha', 'uses' => 'Auth\LoginController@postCriarSenha']);

Route::get('/', ['as' => 'home', 'uses' => 'CommonController@home']);

Route::get('termos-de-uso', function(){ return view('common.termos-de-uso'); });
Route::get('politica-de-privacidade', function(){ return view('common.politica-de-privacidade'); });

Route::group([
'middleware' => ['auth.ativos', 'acesso_padrao']
], function(){
  Route::get('regulamento', ['as' => 'regulamento', 'uses' => 'CommonController@regulamento']);
  Route::get('cronograma', ['as' => 'cronograma', 'uses' => 'CommonController@cronograma']);
  Route::get('avaliadores', ['as' => 'avaliadores', 'uses' => 'CommonController@avaliadores']);
});

Route::group([
'middleware' => ['auth.ativos', 'acesso_medico_coordenador']
], function(){
  Route::get('submeter-caso', ['as' => 'submeter-caso', 'uses' => 'CasosController@submeter']);
  Route::post('submeter-caso', ['as' => 'submeter-caso', 'uses' => 'CasosController@postSubmeter']);
  Route::get('download-formulario', ['as' => 'download-formulario', 'uses' => 'CasosController@downloadFormulario']);
});

Route::group([
'middleware' => ['auth.ativos', 'acesso_avaliador']
], function(){
  Route::get('avaliacoes', ['as' => 'avaliacoes', 'uses' => 'AvaliacoesController@index']);
  Route::get('buscar-avaliacoes', ['as' => 'buscar-avaliacoes', 'uses' => 'AvaliacoesController@buscar']);
  Route::post('enviar-notas', ['as' => 'avaliar', 'uses' => 'AvaliacoesController@enviarNotas']);
});


Route::group([
'middleware' => ['auth.ativos', 'acesso_admin']
], function(){

  Route::get('novos-casos', ['as' => 'novos-casos', 'uses' => 'AdminController@novosCasos']);
  Route::get('download-caso/{caso_codigo}', ['as' => 'download-caso', 'uses' => 'AdminController@downloadCaso']);
  Route::post('revisar-caso', ['as' => 'revisar-caso', 'uses' => 'AdminController@revisarCaso']);
  Route::get('distribuir-caso/{caso_codigo}', ['as' => 'distribuir-caso', 'uses' => 'AdminController@distribuirCaso']);
  Route::post('excluir-caso', ['as' => 'excluir-caso', 'uses' => 'AdminController@excluirCaso']);
  Route::get('status-envios', ['as' => 'status-envios', 'uses' => 'AdminController@statusEnvios']);
  Route::get('historico', ['as' => 'historico', 'uses' => 'AdminController@historico']);
  Route::get('ranking', ['as' => 'ranking', 'uses' => 'AdminController@ranking']);

  Route::get('usuarios', 'AdminController@usuarios');
  Route::get('ativar-usuario/{email}', 'AdminController@ativarUsuario');
  Route::get('iniciar-usuarios', 'AdminController@iniciarUsuarios');

  /*
  Route::get('enviar-todos-casos', function(){
    $casos = Clear\Models\Caso::all();
    foreach($casos as $x => $caso){
      echo $caso->id.' enviado<br>';
      //  ENVIAR CASO
      $ids = $caso->categoria.$caso->coordenador->id.$caso->id;
      $cod = 'C'.str_pad($ids, 6, '0', STR_PAD_LEFT);
      $caso->codigo = $cod;
      $caso->autor = 'Nome Autor teste#'.$x;
      $caso->enviado_em = Carbon\Carbon::now();

      $filename = $cod.'_'.Date('dmYHis').'.pdf';
      $caso->arquivo = $filename;
      $caso->excluido_em = null;
      $caso->save();
    }
  });
  
  Route::get('distribuir-todos', function(){
    $casos = Clear\Models\Caso::all();
    foreach($casos as $x => $caso){
      // DISTRIBUIR CASO
      $avaliadores = Clear\Models\User::getAvaliadores($caso->coordenador->cidade, 2, 16);

      if(count($avaliadores) != 2) {
        echo 'Não foi possível distribuir o caso clínico. (Erro: Total de avaliadores insuficiente (2), selecionados:'.count($avaliadores).')<br>';
      } else {
        foreach ($avaliadores as $avaliador) {
          // Gerar 1 registro de Avaliacao para cada avaliador
          $avaliacao = new Clear\Models\Avaliacao(['casos_id' => $caso->id]);
          $avaliador->avaliacoes()->save($avaliacao);
        }

        $caso->distribuido_em = date('Y-m-d H:i:s');
        $caso->arquivo = null;
        $caso->save();
        echo $caso->id.' distribuido<br>';
      }
    }
  });
  */
});
