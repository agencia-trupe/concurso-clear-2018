<?php

namespace Clear\Http\Controllers\Auth;

use Illuminate\Support\Facades\Auth;
use Clear\Models\User;
use Clear\Http\Controllers\Controller;
use Illuminate\Foundation\Auth\AuthenticatesUsers;
use Illuminate\Http\Request;

class LoginController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Login Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles authenticating users for the application and
    | redirecting them to your home screen. The controller uses a trait
    | to conveniently provide its functionality to your applications.
    |
    */

    use AuthenticatesUsers;

    /**
     * Where to redirect users after login.
     *
     * @var string
     */
    protected $redirectTo = '/';

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest', ['except' => 'logout']);
    }

    public function showLoginForm(Request $request){

      $email = '';

      if($request->login)
        $email = $request->login;

      if(session('flash_login'))
        $email = session('flash_login');

      return view('auth.login', compact('email'));
    }

    public function getCriarSenha($token, $login){
      return view('auth.register', compact('token', 'login'));
    }

    public function postCriarSenha(Request $request){

      $this->validate($request, [
        'email'    => 'required|email|max:255|exists:usuarios,email',
        'token'    => 'required',
        'password' => 'required|min:3|confirmed'
      ]);

      try {

        $usuario = User::where('email', $request->email)->firstOrFail();

        if(is_null($usuario->token_criacao_senha) || $usuario->token_criacao_senha != $request->token)
          return back()->withErrors(['login' => ['Token inválido']]);

        $usuario->password = bcrypt($request->password);
        $usuario->token_criacao_senha = null;
        $usuario->senha_criada_em = Date('Y-m-d H:i:s');
        $usuario->save();

        $request->session()->flash('sucesso_criacao_senha', 'Senha criada com sucesso! Bem-vindo ao sistema do concurso CLEAR de Casos Clínicos.');

        Auth::login($usuario, true);

        return redirect()->to('/');

      } catch (\Exception $e) {

        $erro = logar_erro($e, $request->email);
        return back()->withErrors(array('Não foi possível criar uma nova senha. (Erro: '.$erro.')'));

      }

    }
}
