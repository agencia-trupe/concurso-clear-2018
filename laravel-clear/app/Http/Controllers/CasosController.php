<?php

namespace Clear\Http\Controllers;

use DB;
use Auth;
use Notification;
use Clear\Models\User;
use Clear\Notifications\CasoEnviado;
use Carbon\Carbon as Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Mail;
use Clear\Mail\NotificarFarmacovigilancia;

class CasosController extends Controller{

  public function __construct()
  {
    $this->middleware('auth');
  }

  public function submeter(){

    $casoCat1 = Auth::user()->casos()->categoria('1')->first();
    $casoCat2 = Auth::user()->casos()->categoria('2')->first();
    $casoCat3 = Auth::user()->casos()->categoria('3')->first();

    $usuarioPodeEnviar = $this->userCanSubmit([$casoCat1, $casoCat2, $casoCat3]);

    return view('casos.submeter', [
      'casoCat1' => $casoCat1,
      'casoCat2' => $casoCat2,
      'casoCat3' => $casoCat3,
      'usuarioPodeEnviar' => $usuarioPodeEnviar
    ]);
  }

  /**
   * Limita a submissão de casos para o máximo de 2
   * 
   * @param array $casos
   * 
   * @return bool
   */
  private function userCanSubmit(array $casos) : bool
  {
    if (Auth::user()->tipo == 'medico') {
      return false;
    }

    $casosEnviados = 0;
    
    foreach ($casos as $caso) {
      if ($caso->isEnviado) {
        $casosEnviados++;
      }
    }

    return $casosEnviados < 2;
  }

  public function postSubmeter(Request $request){

    $this->validate($request, [
      '_categoria' => 'required|in:1,2,3|casoNaoEnviado|coordenadorPodeEnviar',
      'arquivo' => 'required|mimes:pdf',
      'autor_principal' => 'required|min:6',
    ]);

    try {

      $caso = Auth::user()->casos()->categoria($request->_categoria)->first();

      $codigo = $this->gerarCodigo(Auth::user()->id, $caso->categoria, $caso->id);
      $caso->codigo = $codigo;
      $caso->autor = $request->autor_principal;
      $caso->coautor_1 = $request->co_autor_1;
      $caso->coautor_2 = $request->co_autor_2;
      $caso->coautor_3 = $request->co_autor_3;
      $caso->coautor_4 = $request->co_autor_4;
      $caso->coautor_5 = $request->co_autor_5;
      $caso->coautor_6 = $request->co_autor_6;
      $caso->enviado_em = Carbon::now();

      $arquivo = $request->arquivo;
      $filename = $codigo.'_'.Date('dmYHis').'.pdf';
      $arquivo->move(env('SITE_ARQUIVOS_DIR'), $filename);
      $caso->arquivo = $filename;
      $caso->excluido_em = null;

      $caso->save();

      $admins = User::admin()->ativos()->get();

      foreach($admins as $adm) {
        $adm->notify(new CasoEnviado($caso, $adm->email));
      }

      Mail::to('farmaco.novartis@novartis.com')->send(new NotificarFarmacovigilancia($caso));

      $request->session()->flash('sucesso', 'Seu caso clínico foi enviado com sucesso!');

      return back();

    } catch (\Exception $e) {

      $request->flash();

      $erro = logar_erro($e, Auth::user()->id);
      return back()->withErrors(array('Erro ao enviar seu caso clínico! (Erro: '.$erro.')'));

    }
  }

  public function downloadFormulario(){
    $titulo = "Formulário relato de caso clínico concurso clear 2017.docm";
    $path = env('SITE_ARQUIVO_FORM');
    return response()->download($path, $titulo);
  }

  private function gerarCodigo($idcoordenador, $idcategoria, $idcaso)
  {
    $ids = $idcategoria.$idcoordenador.$idcaso;
    return 'C'.str_pad($ids, 6, '0', STR_PAD_LEFT);
  }

}
