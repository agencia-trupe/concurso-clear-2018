<?php

namespace Clear\Http\Controllers;

use Auth;
use DB;
use Notification;
use Carbon\Carbon as Carbon;
use Clear\Notifications\UsuarioCriado;
use Clear\Notifications\CasoRemovido;
use Clear\Notifications\CasoDistribuido;
use Clear\Notifications\CasoRevisado;

use Clear\Models\Caso;
use Clear\Models\User;
use Clear\Models\Avaliacao;
use Illuminate\Http\Request;

class AdminController extends Controller
{

  public function __construct()
  {
    $this->middleware('auth');
  }

  public function novosCasos()
  {
    $novosCat1 = Caso::categoria('1')->distribuiveis()->orderBy('enviado_em', 'DESC')->get();
    $novosCat2 = Caso::categoria('2')->distribuiveis()->orderBy('enviado_em', 'DESC')->get();
    $novosCat3 = Caso::categoria('3')->distribuiveis()->orderBy('enviado_em', 'DESC')->get();

    return view('admin.novos-casos', [
      'novosCat1' => $novosCat1,
      'novosCat2' => $novosCat2,
      'novosCat3' => $novosCat3
    ]);
  }

  public function downloadCaso($caso_codigo)
  {
    $caso = Caso::findByCodigoOrFail($caso_codigo);
    $path = env('SITE_ARQUIVOS_DIR').$caso->arquivo;
    return response()->download($path);
  }

  private function selecionarAvaliadores($caso)
  {
    return User::getAvaliadores($caso->coordenador->cidade, 2, 16);
  }

  public function iniciarUsuarios(Request $request)
  {
    $usuarios = User::naoIniciados()->get();

    $body = "";

    if(count($usuarios) > 0){

      foreach($usuarios as $i => $u){
          try {

            $token = str_random(32);


            $u->email_enviado_em = Date('Y-m-d H:i:s');
            $u->token_criacao_senha = $token;
            $u->notify( new UsuarioCriado($token, $u->email));
            $u->save();

            $body .= '<li>E-mail enviado para:'.$u->nome.' ('.$u->email.')</li>';

          } catch (\Exception $e) {
              $body .= '<li class=\'erro\'>Erro ao enviar. ('.$u->email.') '.$e->getMessage().'</li>';
          }
      }

    }else{
      $body .= '<li>Todos os e-mails iniciais para definição de senha já foram enviados.</li>';
    }

    return view('admin.envios', ['body' => $body]);
  }

  public function revisarCaso(Request $request)
  {
    $this->validate($request, [
      '_caso' => 'required|exists:casos,id',
      'arquivo' => 'required|mimes:pdf',
      'justificativa' => 'required|min:3'
    ]);

    try {
      
      $caso = Caso::findOrFail($request->_caso);

      $arquivoOriginal = env('SITE_ARQUIVOS_DIR').$caso->arquivo;

      $caso->revisado_em = date('Y-m-d H:i:s');

      $arquivo = $request->arquivo;
      $filename = $caso->codigo.'_'.Date('dmYHis').'.pdf';
      $arquivo->move(env('SITE_ARQUIVOS_DIR'), $filename);
      $caso->arquivo = $filename;
      $caso->justificativa_revisao = $request->justificativa;

      $caso->save();

      if (file_exists($arquivoOriginal)) {
        unlink($arquivoOriginal);
      }

      $caso->coordenador->notify(new CasoRevisado($caso, $caso->coordenador->email));
      
      $request->session()->flash('sucesso', 'Versão revisada do Caso Clínico armazenada com sucesso.');

      return back();

    } catch (\Exception $e) {
      $request->flash();
      $erro = logar_erro($e, Auth::user()->id);
      return back()->withErrors(array('Não foi possível revisar o caso clínico. (Erro: '.$erro.')'));
    }
  }

  public function distribuirCaso(Request $request, $caso_codigo)
  {
    try {

      $caso = Caso::findByCodigoOrFail($caso_codigo);

      if ($caso->isDistribuido) {
        return back();
      }

      $arquivo = env('SITE_ARQUIVOS_DIR').$caso->arquivo;

      // SELECIONAR AVALIADORES PARA O CASO
      $avaliadores = $this->selecionarAvaliadores($caso);      

      if (count($avaliadores) != 2) {
        return back()->withErrors(
          array('Não foi possível distribuir o caso clínico. (Erro: Total de avaliadores insuficiente (2), selecionados:'.count($avaliadores).')'),
          'share'
        );
      }


      foreach ($avaliadores as $avaliador) {

        // Gerar 1 registro de Avaliacao para cada avaliador
        $avaliacao = new Avaliacao(['casos_id' => $caso->id]);
        $avaliador->avaliacoes()->save($avaliacao);

        // Notificar cada um dos avaliadores (com o arquivo anexo)
        $avaliador->notify(new CasoDistribuido($caso, $avaliador->email));
      }

      $caso->distribuido_em = date('Y-m-d H:i:s');
      $caso->arquivo = null;
      $caso->save();

      if (file_exists($arquivo)) {
        unlink($arquivo);
      }

      $request->session()->flash('sucesso', 'Caso clínico distribuído para os Avaliadores com sucesso.');

      return back();

    } catch (\Exception $e) {

      $erro = logar_erro($e, Auth::user()->id);
      return back()->withErrors(array('Não foi possível distribuir o caso clínico. (Erro: '.$erro.')'), 'share');

    }
  }

  public function excluirCaso(Request $request)
  {
    $this->validate($request, [
      '_caso_delete' => 'required|exists:casos,id',
      'justificativa_exclusao' => 'required|min:3'
    ]);

    try {

      $caso = Caso::findOrFail($request->_caso_delete);
      $arquivo_full = env('SITE_ARQUIVOS_DIR').$caso->arquivo;

      $caso->codigo = null;
      $caso->autor = null;
      $caso->coautor_1 = null;
      $caso->coautor_2 = null;
      $caso->coautor_3 = null;
      $caso->coautor_4 = null;
      $caso->coautor_5 = null;
      $caso->enviado_em = null;

      $caso->arquivo = null;

      $caso->excluido_em = date('Y-m-d H:i:s');
      $caso->justificativa_exclusao = $request->justificativa_exclusao;
      $caso->save();

      if (file_exists($arquivo_full)) {
        unlink($arquivo_full);
      }

      // Notificar Coordenador que Caso foi Removido
      $caso->coordenador->notify(new CasoRemovido($caso, $caso->coordenador->email));

      $request->session()->flash('sucesso', 'Caso clínico removido com sucesso.');

      return back();

    } catch (\Exception $e) {

      $erro = logar_erro($e, Auth::user()->id);
      return back()->withErrors(array('Não foi possível remover o caso clínico. (Erro: '.$erro.')'), 'delete');
    }
  }

  public function statusEnvios()
  {
    $coordenadores = User::coordenadores()->orderBy('nome', 'asc')->get();
    return view('admin.status-envios', compact('coordenadores'));
  }

  public function historico(){
    $historicoCat1 = Caso::categoria(1)->distribuidos()->get();
    $historicoCat2 = Caso::categoria(2)->distribuidos()->get();
    $historicoCat3 = Caso::categoria(3)->distribuidos()->get();
    $avaliadores = User::avaliadores()->orderBy('id', 'asc')->get();

    return view('admin.historico',[
      'historicoCat1' => $historicoCat1,
      'historicoCat2' => $historicoCat2,
      'historicoCat3' => $historicoCat3,
      'avaliadores' => $avaliadores
    ]);
  }

  public function ranking(){

    $rankingCat1 = DB::table('avaliacoes')
                     ->select('casos.*', 'usuarios.nome as coordenador', DB::raw('AVG(avaliacoes.media) as ranking'))
                     ->leftJoin('casos', 'avaliacoes.casos_id', '=', 'casos.id')
                     ->leftJoin('usuarios', 'casos.coordenador_id', '=', 'usuarios.id')
                     ->whereNotNull('avaliacoes.media')
                     ->where('categoria', 1)
                     ->groupBy('avaliacoes.casos_id')
                     ->orderBy('ranking', 'DESC')
                     ->orderBy(DB::raw('AVG(avaliacoes.criterio_3)'), 'DESC') // Critério de desempate: Relato
                     ->orderBy(DB::raw('AVG(avaliacoes.criterio_4)'), 'DESC') // Critério de desempate: Discussão
                     ->orderBy(DB::raw('AVG(avaliacoes.criterio_5)'), 'DESC') // Critério de desempate: Conclusão
                     ->orderBy(DB::raw('AVG(avaliacoes.criterio_2)'), 'DESC') // Critério de desempate: Introdução
                     ->get();

    $rankingCat2 = DB::table('avaliacoes')
                    ->select('casos.*', 'usuarios.nome as coordenador', DB::raw('AVG(avaliacoes.media) as ranking'))
                    ->leftJoin('casos', 'avaliacoes.casos_id', '=', 'casos.id')
                    ->leftJoin('usuarios', 'casos.coordenador_id', '=', 'usuarios.id')
                    ->whereNotNull('avaliacoes.media')
                    ->where('categoria', 2)
                    ->groupBy('avaliacoes.casos_id')
                    ->orderBy('ranking', 'DESC')
                    ->orderBy(DB::raw('AVG(avaliacoes.criterio_3)'), 'DESC') // Critério de desempate: Relato
                    ->orderBy(DB::raw('AVG(avaliacoes.criterio_4)'), 'DESC') // Critério de desempate: Discussão
                    ->orderBy(DB::raw('AVG(avaliacoes.criterio_5)'), 'DESC') // Critério de desempate: Conclusão
                    ->orderBy(DB::raw('AVG(avaliacoes.criterio_2)'), 'DESC') // Critério de desempate: Introdução
                    ->get();

    $rankingCat3 = DB::table('avaliacoes')
                    ->select('casos.*', 'usuarios.nome as coordenador', DB::raw('AVG(avaliacoes.media) as ranking'))
                    ->leftJoin('casos', 'avaliacoes.casos_id', '=', 'casos.id')
                    ->leftJoin('usuarios', 'casos.coordenador_id', '=', 'usuarios.id')
                    ->whereNotNull('avaliacoes.media')
                    ->where('categoria', 3)
                    ->groupBy('avaliacoes.casos_id')
                    ->orderBy('ranking', 'DESC')
                    ->orderBy(DB::raw('AVG(avaliacoes.criterio_3)'), 'DESC') // Critério de desempate: Relato
                    ->orderBy(DB::raw('AVG(avaliacoes.criterio_4)'), 'DESC') // Critério de desempate: Discussão
                    ->orderBy(DB::raw('AVG(avaliacoes.criterio_5)'), 'DESC') // Critério de desempate: Conclusão
                    ->orderBy(DB::raw('AVG(avaliacoes.criterio_2)'), 'DESC') // Critério de desempate: Introdução
                    ->get();

    $datamax = Carbon::createFromFormat('d/m/Y', env('SITE_PUBLICACAO_RANKING'));
    $hoje = Carbon::now();

    return view('admin.ranking', [
      'rankingCat1' => $rankingCat1,
      'rankingCat2' => $rankingCat2,
      'rankingCat3' => $rankingCat3,
      'publicarRanking' => $hoje->gte($datamax)
    ]);
  }

  public function usuarios(){
    $usuarios = User::orderBy('nome', 'asc')->get();
    return view('admin.usuarios', compact('usuarios'));
  }

  public function ativarUsuario(Request $request, $email){
    $token = str_random(32);

    $usuario = User::where('email', $email)->firstOrFail();
    $usuario->email_enviado_em = Date('Y-m-d H:i:s');
    $usuario->token_criacao_senha = $token;
    $usuario->notify( new UsuarioCriado($token, $usuario->email));
    $usuario->save();

    $request->session()->flash('sucesso', 'E-mail enviado com sucesso');

    return back();
  }

}
