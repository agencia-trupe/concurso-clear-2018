<?php

if ( ! function_exists('media_ponderada'))
{
	function media_ponderada($nota1, $nota2, $nota3, $nota4, $nota5)
	{
		$peso1 = 1;
		$peso2 = 1;
		$peso3 = 3;
		$peso4 = 3;
		$peso5 = 2;

		$criterio_1 = $nota1 * $peso1;
    	$criterio_2 = $nota2 * $peso2;
    	$criterio_3 = $nota3 * $peso3;
		$criterio_4 = $nota4 * $peso4;
		$criterio_5 = $nota5 * $peso5;

		// média ponderada normalizada
		//$soma_pesos = $peso1 + $peso2 + $peso3 + $peso4 + $peso5;
		//return ($criterio_1 + $criterio_2 + $criterio_3 + $criterio_4)/$soma_pesos;

		// somatória ponderada
		return ($criterio_1 + $criterio_2 + $criterio_3 + $criterio_4 + $criterio_5);
	}
}

if ( ! function_exists('logar_erro'))
{
	function logar_erro($e, $id)
	{
		$msg = $e->getMessage().' (arquivo: '.$e->getFile().' - linha: '.$e->getLine().') '.$e->getCode();
		$hash = intval($id.$msg, 36);

		DB::table('log_erros')->insert([
			'hash' => $hash,
			'usuario' => $id,
			'msg' => $msg,
			'created_at' => Carbon\Carbon::now()
		]);

		return $hash;
	}
}


if ( ! function_exists('caso_avaliavel_por'))
{
	function caso_avaliavel_por($caso_id, $avaliador_id)
	{
		$query = DB::table('avaliacoes')->where([
			'casos_id' => $caso_id,
			'avaliador_id' => $avaliador_id
		])->count();

		return $query > 0;
	}
}


/**
 * Returns the base url of the app
 *
 * @access public
 * @param  string		$url
 * @return string
 */
if ( ! function_exists('base_url'))
{
	function base_url($url = '')
	{
		return 'http://'.$_SERVER['HTTP_HOST'].'/'.$url;
	}
}

/**
 * Sorts a collection using natural sort
 *
 * @access public
 * @param  Collection  $collection
 * @return Collection
 */
if ( ! function_exists('collection_natsort'))
{
	function collection_natsort($collection){
		$items = $collection->all();
		$list = $collection->lists('titulo');

		array_multisort($list, SORT_ASC, SORT_NATURAL, $items);

		return new Collection(array_values($items));;
	}
}


/**
 * Limit the number of words in a string.
 *
 * @access	public
 * @param  string  $value
 * @param  int     $words
 * @param  string  $end
 * @return string
 */
if ( ! function_exists('str_words'))
{
	function str_words($value, $words = 100, $end = '...')
	{
		preg_match('/^\s*+(?:\S++\s*+){1,'.$words.'}/u', $value, $matches);

		if ( ! isset($matches[0])) return $value;

		if (strlen($value) == strlen($matches[0])) return $value;

		return rtrim($matches[0]).$end;
	}
}

/**
 *
 * Simply adds the http:// part if no scheme is included
 *
 * @access	public
 * @param	string	$str
 * @return	string
 */
if ( ! function_exists('prep_url'))
{
	function prep_url($str = '')
	{
		if ($str == 'https://' OR $str == 'http://' OR $str == '')
		{
			return '';
		}

		$url = parse_url($str);

		if ( ! $url OR ! isset($url['scheme']))
		{
			$str = 'http://'.$str;
		}

		return $str;
	}
}

/**
 *
 * Generate embed code to a given 'google maps incorporation code'
 *
 * @access	public
 * @param	string	$str
 * @param	string  $width
 * @param	string  $height
 * @param	string  $classe
 * @return	string
 */
if ( ! function_exists('embed_maps'))
{
	function embed_maps($str = '', $width = '', $height = '', $classe = '')
	{

	    //$str = stripslashes(htmlspecialchars_decode($str));

	    if($width != '')
	        $str = preg_replace("~width=\"(\d+)\"~", 'width="'.$width.'"', $str);
	    else
	    	$str = preg_replace("~width=\"(\d+)\"~", '', $str);

	    if($height != '')
	        $str = preg_replace("~height=\"(\d+)\"~", 'height="'.$height.'"', $str);
	    else
	    	$str = preg_replace("~height=\"(\d+)\"~", '', $str);

	    if($classe != '')
	    	$str = preg_replace("~<iframe~", "<iframe class='{$classe}'", $str);

		// COM o link 'ver mapa ampliado'
	    // return $str;

	    // SEM o link 'ver mapa ampliado'
	    return preg_replace('~<br \/>(.*)~', '', $str);
	}
}

/**
 *
 * Return client's IP address
 *
 * @access	public
 * @return	string
 */
if ( ! function_exists('ip'))
{
	function ip()
	{
		if (!empty($_SERVER['HTTP_CLIENT_IP']))
		  return $_SERVER['HTTP_CLIENT_IP'];
		elseif (!empty($_SERVER['HTTP_X_FORWARDED_FOR']))
		  return $_SERVER['HTTP_X_FORWARDED_FOR'];
		else
		  return $_SERVER['REMOTE_ADDR'];
	}
}
