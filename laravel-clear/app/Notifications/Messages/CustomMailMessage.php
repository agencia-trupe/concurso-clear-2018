<?php

namespace Clear\Notifications\Messages;

use Illuminate\Notifications\Messages\MailMessage;

class CustomMailMessage extends MailMessage
{

    /**
     * The html lines of the notification.
     * 
     * @var string 
     */
    public $html;

    /**
     * The thank you note of the notification.
     * 
     * @var string 
     */
    public $thanks;

    /**
     * The reminder text of the notification.
     * 
     * @var string 
     */
    public $reminder;

    /**
     * Set the html of the notification.
     *
     * @param  string  $html
     * @return $this
     */
    public function html(string $html)
    {
        $this->html = $html;

        return $this;
    }

    /**
     * Set the Reminder text of the notification.
     *
     * @param  string  $reminder
     * @return $this
     */
    public function reminder(string $reminder)
    {
        $this->reminder = $reminder;

        return $this;
    }    

    /**
     * Set the thank you note of the notification.
     *
     * @param  string  $thanks
     * @return $this
     */
    public function thanks(string $thanks)
    {
        $this->thanks = $thanks;

        return $this;
    }    

    /**
     * Get an array representation of the message.
     *
     * @return array
     */
    public function toArray()
    {
        return [
            'level'      => $this->level,
            'subject'    => $this->subject,
            'greeting'   => $this->greeting,
            'salutation' => $this->salutation,
            'introLines' => $this->introLines,
            'outroLines' => $this->outroLines,
            'html'       => $this->html,
            'thanks'     => $this->thanks,
            'actionText' => $this->actionText,
            'actionUrl'  => $this->actionUrl,
            'reminder'   => $this->reminder
        ];
    }
}