<?php

namespace Clear\Notifications;

use Illuminate\Bus\Queueable;
use Illuminate\Notifications\Notification;
use Illuminate\Contracts\Queue\ShouldQueue;
use Clear\Notifications\Messages\CustomMailMessage;

class CasoRevisado extends Notification
{
    use Queueable;

    protected $caso;
    protected $email;

    /**
     * Create a new notification instance.
     *
     * @return void
     */
    public function __construct($caso, $email)
    {
        $this->caso = $caso;
        $this->email = $email;
    }

    /**
     * Get the notification's delivery channels.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function via($notifiable)
    {
        return ['mail'];
    }

    /**
     * Get the mail representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return \Illuminate\Notifications\Messages\MailMessage
     */
    public function toMail($notifiable)
    {
        return (new CustomMailMessage)
                    ->subject('Concurso CLEAR - Caso clínico revisado')
                    ->level('success')
                    ->greeting('Prezado Coordenador (a),')
                    ->line('A comissão organizadora do concurso CLEAR 2018 identificou que seu caso clínico contém informações que podem identificar o médico, e/ou o paciente, tomamos a liberdade de remover essas informações de modo a garantir a imparcialidade do concurso e da avaliação, conforme descrito no regulamento.')
                    ->line('Anexo segue o arquivo alterado para sua ciência. Garantimos também que nenhuma informação relevante do caso clínico foi removida.')
                    ->line('Abaixo encontram-se descritas as alterações realizadas:')
                    ->html("<quote style='display:block; padding: 10px 20px; margin: 15px 0 30px 0; font-style: italic; border-left: 5px #CCC solid;'>".$this->caso->justificativa_revisao."</quote>")
                    ->reminder('Garantimos também que não haverá perda de pontuação em decorrência desta revisão.')
                    ->thanks('Obrigado,')
                    ->attach(env('SITE_ARQUIVOS_DIR').$this->caso->arquivo);
    }

    /**
     * Get the array representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function toArray($notifiable)
    {
        return [
            //
        ];
    }
}

