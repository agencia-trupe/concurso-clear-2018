<?php

namespace Clear\Notifications;

use Illuminate\Bus\Queueable;
use Illuminate\Notifications\Notification;
use Illuminate\Contracts\Queue\ShouldQueue;
use Clear\Notifications\Messages\CustomMailMessage;

class CasoRemovido extends Notification
{
    use Queueable;

    protected $caso;
    protected $email;

    /**
     * Create a new notification instance.
     *
     * @return void
     */
    public function __construct($caso, $email)
    {
        $this->caso = $caso;
        $this->email = $email;
    }

    /**
     * Get the notification's delivery channels.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function via($notifiable)
    {
        return ['mail'];
    }

    /**
     * Get the mail representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return \Illuminate\Notifications\Messages\MailMessage
     */
    public function toMail($notifiable)
    {
      $link = url(config('app.url').route('submeter-caso', [
        'login' => $this->email
      ], false));

      return (new CustomMailMessage)
                ->subject('Concurso CLEAR - Caso clínico removido')
                ->level('error')
                ->greeting('Caso clínico removido')
                ->line('Seu arquivo foi rejeitado pela Administração do Concurso Clear de Casos Clínicos.')
                ->action('Reenviar Caso', $link)
                ->line('Acesse o sistema para reenviar este caso para o concurso.')
                ->html("<quote style='display:block; padding: 10px 20px; margin: 15px 0 30px 0; font-style: italic; border-left: 5px #CCC solid;'>Justificativa: ".$this->caso->justificativa_exclusao."</quote>");
    }

    /**
     * Get the array representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function toArray($notifiable)
    {
        return [
            //
        ];
    }
}
