<?php

namespace Clear\Notifications;

use Illuminate\Bus\Queueable;
use Illuminate\Notifications\Notification;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Notifications\Messages\MailMessage;

class ResetarSenha extends Notification
{
    use Queueable;

    public $token;
    public $login;

    /**
     * Create a new notification instance.
     *
     * @return void
     */
    public function __construct($token, $login)
    {
        $this->token = $token;
        $this->login = $login;
    }

    /**
     * Get the notification's delivery channels.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function via($notifiable)
    {
        return ['mail'];
    }

    /**
     * Get the mail representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return \Illuminate\Notifications\Messages\MailMessage
     */
    public function toMail($notifiable)
    {
      $link = url(config('app.url').route('password.reset', [
        'token' => $this->token,
        'email' => $this->login,
      ], false));

        return (new MailMessage)
              ->subject('Concurso CLEAR - Redefinição de Senha')
              ->greeting('Redefinição de Senha')
              ->line('Você está recebendo este e-mail pois foi solicitada uma troca de senha para esta conta.')
              ->action('Redefinir Senha', $link)
              ->line('Caso não tenha solicitado uma nova senha, nenhuma ação é necessária.');
    }

}
