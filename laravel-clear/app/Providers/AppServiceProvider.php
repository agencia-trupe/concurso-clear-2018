<?php

namespace Clear\Providers;

use Auth;
use Validator;
use Illuminate\Support\Facades\Schema;
use Illuminate\Support\ServiceProvider;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
      Validator::extend('casoNaoEnviado', function ($attribute, $value, $parameters, $validator) {
        $caso = Auth::user()->casos()->categoria($value)->first();
        return !$caso->isEnviado;
      });

      Validator::extend('coordenadorPodeEnviar', function ($attribute, $value, $parameters, $validator) {
        $caso = Auth::user()->casos()->categoria($value)->first();
        return $caso->podeSerEnviado;
      });

      Validator::extend('notaValida', function ($attribute, $nota, $parameters, $validator) {
        // retorna true se nota for FLOAT de 0.0 até 10.0 com uma casa decimal
        return $nota >= 0 && $nota <= 10;
      });

      Validator::extend('avaliacaoPodeSerAtualizada', function ($attribute, $value, $parameters, $validator) {
        // retorna true se avaliador logado é dono da avaliação e está dentro da data max da avaliação
        $avaliacao_id = $value;
        $avaliacao = \Clear\Models\Avaliacao::findOrFail($avaliacao_id);

        if(Auth::user()->id != $avaliacao->avaliador_id)
          return false;

        return $avaliacao->podeSerAvaliado;
      });

    }

    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        //
    }
}
