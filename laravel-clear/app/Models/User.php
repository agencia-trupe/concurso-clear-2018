<?php

namespace Clear\Models;

use Clear\Notifications\ResetarSenha;
use Clear\Notifications\UsuarioCriado;
use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;

class User extends Authenticatable
{

  use Notifiable;

  protected $table = 'usuarios';

  protected $fillable = [
    'tipo',
    'nome',
    'crm',
    'is_ativo',
    'cidade',
    'estado',
    'email',
    'password'
  ];

  protected $hidden = [
    'password',
    'remember_token'
  ];

  protected $dates = [
    'senha_criada_em',
    'email_enviado_em',
    'created_at',
    'updated_at'
  ];

  /**
   * Send the password reset notification.
   *
   * @param  string  $token
   * @return void
   */
  public function sendPasswordResetNotification($token)
  {
    $this->notify(new ResetarSenha($token, $this->email));
  }

  public function sendPasswordCreationNotification($token)
  {
    $this->notify(new UsuarioCriado($token, $this->email));
  }

  // COMPUTED ATTRIBUTES

  public function getTipoExtensoAttribute()
  {
    return $this->tipo == 'medico' ? 'Médico' : ucfirst($this->tipo);
  }

  public function getIsMedicoAttribute()
  {
    return $this->tipo == 'medico';
  }

  public function getIsCoordenadorAttribute()
  {
    return $this->tipo == 'coordenador';
  }

  public function getIsAvaliadorAttribute()
  {
    return $this->tipo == 'avaliador';
  }

  public function getIsAdminAttribute()
  {
    return $this->tipo == 'admin';
  }

  public function getIsAtivoAttribute()
  {
    return !is_null($this->senha_criada_em);
  }

  public function getPodeAvaliarAttribute($caso_id)
  {
    // verificar se tem uma avaliação pra esse avaliador e id de caso
    return sizeof($this->avaliacoes()->where('casos_id', $caso_id)->get()) > 0 ? true : false;
  }

  public function getStatusEnvioCaso1Attribute()
  {
    $caso = $this->casos()->categoria(1)->first();

    return is_null($caso->enviado_em) ?
            "<strong>PENDENTE</strong>" :
            "ENVIADO EM: ".$caso->enviado_em->format('d/m/Y');
  }

  public function getStatusEnvioCaso2Attribute()
  {
    $caso = $this->casos()->categoria(2)->first();

    return is_null($caso->enviado_em) ?
            "<strong>PENDENTE</strong>" :
            "ENVIADO EM: ".$caso->enviado_em->format('d/m/Y');
  }

  public function getStatusEnvioCaso3Attribute()
  {
    $caso = $this->casos()->categoria(3)->first();

    return is_null($caso->enviado_em) ?
            "<strong>PENDENTE</strong>" :
            "ENVIADO EM: ".$caso->enviado_em->format('d/m/Y');
  }  

  // SCOPES

  public function scopeAdmin($query)
  {
    return $query->where('tipo', 'admin');
  }

  public function scopeAvaliadores($query)
  {
    return $query->where('tipo', 'avaliador');
  }

  public function scopeCoordenadores($query)
  {
    return $query->where('tipo', 'coordenador');
  }

  public function scopeFiltrarLocal($query, $cidade)
  {
    return $query->where('cidade', '!=', $cidade);
  }

  public function scopeAtivos($query)
  {
    return $query->whereNotNull('senha_criada_em');
  }

  public function scopeNaoIniciados($query)
  {
    return $query->whereNull('email_enviado_em');
  }

  public function scopeOrdenarPorMenosAvaliacoes($query)
  {
    return $query->withCount('avaliacoes')
                 ->orderBy('avaliacoes_count', 'asc');
  }

  static public function getAvaliadores($cidade, $quantidade, $maxAvaliacoes)
  {
    $avaliadores = self::avaliadores()
                      ->filtrarLocal($cidade)
                      ->ordenarPorMenosAvaliacoes()
                      ->get();
    
    $selecionados = $avaliadores->filter( function($avaliador) use ($maxAvaliacoes) {
      return $avaliador->avaliacoes_count < $maxAvaliacoes;
    });
    
    return $selecionados->slice(0, $quantidade);
  }

  // RELATIONSHIPS

  public function casos()
  {
    return $this->hasMany('Clear\Models\Caso', 'coordenador_id');
  }

  public function avaliacoes()
  {
    return $this->hasMany('Clear\Models\Avaliacao', 'avaliador_id');
  }

}
