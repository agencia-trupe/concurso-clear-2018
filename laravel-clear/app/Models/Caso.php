<?php

namespace Clear\Models;

use Carbon\Carbon as Carbon;
use Illuminate\Database\Eloquent\Model;

class Caso extends Model
{

  protected $table = 'casos';

  protected $fillable = [
    //'coordenador_id',
    //'categoria',
    'codigo',
    'arquivo',
    'autor',
    'coautor_1',
    'coautor_2',
    'coautor_3',
    'coautor_4',
    'coautor_5',
    'coautor_6',
    'enviado_em',
    'distribuido_em',
    'excluido_em'
  ];

  protected $hidden = [
    //'codigo',
    'arquivo'
  ];

  protected $dates = [
    'enviado_em',
    'distribuido_em',
    'excluido_em',
    'created_at',
    'updated_at'
  ];

  // COMPUTED ATTRIBUTES

  public function getIsEnviadoAttribute()
  {
    return is_null($this->enviado_em) ? false : true;
  }

  public function getIsDistribuidoAttribute()
  {
    return is_null($this->distribuido_em) ? false : true;
  }

  public function getPodeSerEnviadoAttribute()
  {
    return env('SITE_ACEITAR_SUBMISSOES', false);
    // $datamax = Carbon::createFromFormat('d/m/Y', env('SITE_DATA_MAX_ENVIO'));
    // $hoje = Carbon::now();
    // return $hoje->lte($datamax);
  }

  public function getPontuacaoAttribute()
  {
    $avaliacoes = $this->avaliacoes()->avaliados()->get();
    $total = 0;

    foreach($avaliacoes as $aval)
      $total = $total + $aval->media;

    return $total;
  }

  public function getIsSemCoAutoresAttribute()
  {
    return empty($this->coautor_1)
           && empty($this->coautor_2)
           && empty($this->coautor_3)
           && empty($this->coautor_4)
           && empty($this->coautor_5)
           && empty($this->coautor_6);
  }

  // SCOPES

  public function scopeCategoria($query, $value)
  {
    return $query->where('categoria', $value);
  }

  public function scopeDistribuiveis($query)
  {
    return $query->whereNotNull('enviado_em')
                 ->whereNull('distribuido_em');
  }

  public function scopeDistribuidos($query)
  {
    return $query->whereNotNull('enviado_em')
                 ->whereNotNull('distribuido_em');
  }

  public function scopeAvaliados($query)
  {
    return $query->whereNotNull('enviado_em')
                 ->whereNotNull('distribuido_em')
                 ->whereHas('avaliacoes', function($query){
                   $query->avaliados();
                 });
  }

  public function scopeFindByCodigoOrFail($query, $codigo)
  {
    return $query->where('codigo', $codigo)->firstOrFail();
  }

  public function scopeOrdenarPorRanking($query)
  {
    return $query;
  }

  // RELATIONSHIPS

  public function avaliacoes()
  {
    return $this->hasMany('Clear\Models\Avaliacao', 'casos_id');
  }

  public function coordenador()
  {
    return $this->belongsTo('Clear\Models\User', 'coordenador_id');
  }

}
