<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AlterCasosTableRevision extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('casos', function (Blueprint $table) {
            $table->datetime('revisado_em')->nullable()->after('excluido_em');
            $table->text('justificativa_revisao')->nullable()->after('revisado_em');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('casos', function (Blueprint $table) {
            $table->dropColumn('revisado_em');
            $table->dropColumn('justificativa_revisao');
        });
    }
}
