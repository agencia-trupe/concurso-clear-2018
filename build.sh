#!/bin/bash

# Build do Sistema para produção

rm -Rf build
cd laravel-clear
composer install
cd ..
npm install
npm run production

mysqldump -h 172.25.0.3 -u clear -pclear clear > dump-prod.sql

mkdir build
chmod 0777 -R build
cp -R laravel-clear/app build/laravel-clear/
cp -R laravel-clear/bootstrap build/laravel-clear/
cp -R laravel-clear/config build/laravel-clear/
cp -R laravel-clear/database build/laravel-clear/
cp -R laravel-clear/resources build/laravel-clear/
cp -R laravel-clear/routes build/laravel-clear/
cp -R laravel-clear/storage build/laravel-clear/
cp -R laravel-clear/tests build/laravel-clear/
cp -R laravel-clear/vendor build/laravel-clear/
cp -R laravel-clear/.env build/laravel-clear/
cp -R laravel-clear/.htaccess build/laravel-clear/
cp -R laravel-clear/artisan build/laravel-clear/
cp -R laravel-clear/composer.json build/laravel-clear/
cp -R laravel-clear/composer.lock build/laravel-clear/
cp -R laravel-clear/phpunit.xml build/laravel-clear/
cp -R laravel-clear/server.php build/laravel-clear/

cp -R public build/
cp .gitattributes build/
cp .gitignore build/
cp mix-manifest.json build/
cp webpack.mix.js build/
cp package-lock.json build/
cp package.json build/
cp laravel-clear/.env.producao build/.env
cp dump-prod.sql build/
cp INSTALL.md build/
